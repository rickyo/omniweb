<?php

include_once("config.php");
include_once("connection.php");


if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Omni-Web Trading Plan</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/JSONedtr.js"></script>
<link rel="stylesheet" type="text/css" href="css/JSONedtr.css">

<script>
var tradingPlanVar;
var toggleHide = false;
//all on
//var defaultData = '[{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-200))<=30&&bigBuyForce>1","profitThreshold":"150","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"true","desc":"vwap-2 long"},{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-100))<=30&&bigBuyForce>1","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"true","desc":"vwap-1 long"},{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-0))<=30&&bigBuyForce>1","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"LONG","enable":"true","desc":"vwap0 long"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+100))<=30&&bigSellForce>1","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"SHORT","enable":"true","desc":"vwap+1 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+200))<=30&&bigSellForce>1","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"SHORT","enable":"true","desc":"vwap+2 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&bigSellForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"SHORT","enable":"true","desc":"follow big sell"},{"conditonExpression":"msaPosition==\\"LONG\\"&&bigBuyForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"LONG","enable":"true","desc":"follow big buy"}]';
//all off
var defaultData = '[{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-200))<=30&&bigBuyForce>1","profitThreshold":"150","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"vwap-2 long"},{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-100))<=30&&bigBuyForce>1","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"vwap-1 long"},{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-0))<=30&&bigBuyForce>1","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"vwap0 long"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+100))<=30&&bigSellForce>1","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"vwap+1 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+200))<=30&&bigSellForce>1","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"vwap+2 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&bigSellForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"follow big sell"},{"conditonExpression":"msaPosition==\\"LONG\\"&&bigBuyForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"follow big buy"}]';
//var defaultData ='[{"conditonExpression":"(bigBuyForce>100||isMSAConfirm)&&msaPosition==\\"LONG\\"","profitThreshold":50,"lossThreshold":20,"trailingStopRatio":90,"pos":"LONG","enable":false},{"conditonExpression":"(bigSellForce>100||isMSAConfirm)&&msaPosition==\\"SHORT\\"","profitThreshold":50,"lossThreshold":20,"trailingStopRatio":90,"pos":"SHORT","enable":false},{"conditonExpression":"bigBuyForce>50&&msaPosition==\\"LONG\\"","profitThreshold":50,"lossThreshold":20,"trailingStopRatio":90,"pos":"LONG","enable":false},{"conditonExpression":"bigSellForce>50&&msaPosition==\\"SHORT\\"","profitThreshold":50,"lossThreshold":20,"trailingStopRatio":90,"pos":"SHORT","enable":false},{"conditonExpression":"Math.abs(future-vwap)<=3&&isMSASupport","profitThreshold":80,"lossThreshold":30,"trailingStopRatio":90,"pos":"LONG","enable":false},{"conditonExpression":"Math.abs(future-(vwap+200))<=3&&isMSAResist","profitThreshold":120,"lossThreshold":150,"trailingStopRatio":90,"pos":"SHORT","enable":false},{"conditonExpression":"Math.abs(future-(vwap-200))<=3&&isMSASupport","profitThreshold":120,"lossThreshold":150,"trailingStopRatio":50,"pos":"LONG","enable":false}]';
//follow trend break through
//var defaultData = '{"strategy":[{"conditonExpression":"msaPosition==\\"LONG\\"&&Math.abs(future-(vwap-200))<=30&&bigBuyForce>1","profitThreshold":"150","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"vwap-2 long"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&bigBuyForce>10","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"LONG","enable":"true","desc":"vwap-1 long"},{"conditonExpression":"msaPosition==\\"LONG\\"&&bigSellForce>10","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"SHORT","enable":"true","desc":"vwap0 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+100))<=30&&bigSellForce>1","profitThreshold":"70","lossThreshold":"30","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"vwap+1 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&Math.abs(future-(vwap+200))<=30&&bigSellForce>1","profitThreshold":"100","lossThreshold":"50","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"vwap+2 short"},{"conditonExpression":"msaPosition==\\"SHORT\\"&&bigSellForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"SHORT","enable":"false","desc":"follow big sell"},{"conditonExpression":"msaPosition==\\"LONG\\"&&bigBuyForce>50","profitThreshold":"50","lossThreshold":"20","trailingStopRatio":"90","pos":"LONG","enable":"false","desc":"follow big buy"}]}';
var tradingPlanVar="";
$(document).ready(function(){

	loadDefault();
	$("#update").click(function(){
		var cmd = toStrategyString();
		//alert(cmd);
		var url = "OmniController.php"; 
		var data = {};
		data.action = "trading_plan_config";
		data.host = "<?php echo $host; ?>",
		data.port = "<?php echo $port;?>",
		data.userid = "<?php echo $g_userid;?>"
		data.value = cmd;
		data.sync = false;
		
		$.post(
			url,
			data,
			function(json) {
				
				if (json.ok == 1&&json.rst!=null){
					
					alert(json.value);
					
				}else{
					alert("Fail! Please check syntax");
				}
			}
		).fail(function(){
			alert("fail!");
		}).always(function(){checkDisable();$(this).blur();});;
	})
	
	$("#load").click(function(){
		loadDefault()
		$(this).blur();
	})
	
	$("#toggle").click(function(){
		if(toggleHide){
			toggleHide = false;
		}else{
			toggleHide = true;
		}
		checkDisable();
		$(this).blur();
	})
	
	$("#defaultSetting").click(function(){
		 tradingPlanVar = new JSONedtr( defaultData, '#output' );
		 checkDisable();
		 $(this).blur();
	})
})

function loadDefault(){
	var url = "OmniController.php"; 
		var data = {};
		data.action = "trading_plan_config";
		data.host = "<?php echo $host; ?>",
		data.port = "<?php echo $port;?>",
		data.userid = "<?php echo $g_userid;?>"
		data.value = true;
		data.sync = false;
		
		$.post(
			url,
			data,
			function(json) {
				
				if (json.ok == 1){
					var data = JSON.stringify(json.rst.strategy);
					tradingPlanVar = new JSONedtr( data, '#output' );
					alert("Done");
					
				}
			}
		).fail(function(){
			alert("fail!");
		}).always(function(){checkDisable()});
}
function toStrategyString(){
	var rst = {"strategy":[]};
	if(tradingPlanVar!=""){
		var dataMap = tradingPlanVar.getData();
		for(i=0;i<20;i++){
			if(dataMap[i]!='undefined'&&dataMap[i]!=null){
				rst.strategy.push(dataMap[i]);
			}else{
				break;
			}
		}
	}
	return JSON.stringify(rst);
	
}

function checkDisable(){
	$("input[data-key='enable']").each(function(){
		var enable = $(this).val();
		var classVal = toggleHide?"hide disable":"disable";
		$(this).parent().parent().removeClass("hide disable");
		if(enable=="false"){
			$(this).parent().parent().addClass(classVal);
		}
	})
}

</script>
<style>
	#help{
		margin: 50px 50px 0px;
	}
	
	 #control.jse--output {
		float:right;
		min-height:20px;
	}
	
	.disable input.jse--value{
		color:grey;
	}
	
	.jse--row.jse--row--array.hide{
	
		display:none;
	}

</style>
</head>
<body >


	<div id="output"></div>
	<div id="control" class="jse--output">
		<input type="button" id="update" value="Update" ></input>
		<input type="button" id="load" value="Load" ></input>
		<input type="button" id="defaultSetting" value="Default Setting" ></input>
		<input type="button" id="toggle" value="Toggle Hidden" ></input>
	</div>
</body>
</html>