<?php

ini_set("allow_url_fopen", 1);
include_once("config.php");
include_once("connection.php");

$g_userid = "";
$host = "";
$ip = "";
$port = "";
if (isset($_GET) && is_array($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}

$domain = "https://www.altodock.com";

//$url = "http://localhost/majestic/omni/user/user_account.php?userid=".$g_userid;
$url = $domain."/majestic/omni/user_account.php?userid=".$g_userid;
//$url = "http://veeko123.ddns.net/majestic/omni/user_account.php?userid=".$g_userid;

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);
$result = curl_exec($ch);
curl_close($ch);

global $obj;

$obj = json_decode($result);


// no profile search, set parent profile
if (!$obj || sizeof((array)$obj) == 0){
	$obj = array();
	$obj["acc_id"] = 9999;
	$obj["userid"] = $g_userid;
	$obj["parentid"] = 0;
	$obj["viewonly"] = 0;
	$obj["server"] =  $_GET["host"];
	$obj["port"] =  $_GET["port"];
	$obj["acc_type"] = 1;
	$obj["status"] = 1;
	
	$obj_parent = array();
	$obj_parent[$g_userid] = (object)$obj;
	$obj = $obj_parent;
}

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link rel="stylesheet" href="alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />
	<link href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


</head>

<style>


.clickable{
	display:none;
}

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#000;
	color:#FFF;
	max-width :470px;
	overflow-x:hidden;
}

.green{
	color:#00CC00;
}

.red{
	color:#CC0000;
}

.grey{
	color :#808080;
}

.container{
	width:100%;
	max-width :470px;
	padding:20px;
}

.align-left{
	float:left;
	width:40%;
}

.align-right{
	float:right;
	width:60%;
}

.clearfix{
	clear:both;
}

.market_price{
	font-size:26px;
	font-weight: bold;
}

.market_price_net{
	font-size:18px;
}

.market_info tr> td{
	font-size:16px;
}
.market_info .offset-left-30{
	padding-left:20px;
}

.header{
	padding-top:20px;
}

.account_list{
	margin-top:30px;
	border-top : 1px solid #CCCCCC;
}

.account_list .row{
	min-height : 70px;
	border-bottom : 1px solid #CCCCCC;
	padding:20px;
}



.account_list .row .user, .account_list .row  .trade_status, .account_list .row .point , .account_list .row .balance{
	
	display:inline-block;
}
.account_list .row .user{
	width:300px;
}
.account_list .row .trade_status{
	width:200px ;
}
.account_list .row .point , .account_list .row .balance{
	width:80px;
}
.switch-wrapper {
  display: inline-block;
  position: relative;
  top: 3px;
}

.switch-button-label {
    float: left;

    font-size: 10pt;
    cursor: pointer;
}

.switch-button-label.off {
    color: #adadad;
}

.switch-button-label.on {
    color: #0088CC;
}

.switch-button-background {
    float: left;
    position: relative;

    background: #ccc;
    border: 1px solid #aaa;

    margin: 1px 10px;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;

    cursor: pointer;
}

.switch-button-button {
    position: absolute;

    left: -1px;
    top : -1px;

    background: #FAFAFA;
    border: 1px solid #aaa;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

.btn{
	text-align: center;
	border-radius:3px;	
	height : 20px;
	width :65px;
	padding:10px;
	line-height:1px;
	margin-right:13px;
	cursor:pointer;
}


.btncall{
	background-color : #00CC00;
	border : 3px solid #00CC00;
	color:#FFF;
}

.btncall.on, .btncall:hover{
	background-color : #065600;
	color:#FFF;
}

.btnput{
	background-color : #CC0000;
	border : 3px solid #CC0000;
	color:#FFF;
}

.btnput.on, .btnput:hover{
	background-color : #5f0900;
	color:#FFF;
}
.btnsettle{
	background-color : #007eff;
	border : 3px solid #007eff;
	color:#FFF;
}
.btnsettle.on,  .btnsettle:hover{
	background-color : #002da1;
	color:#FFF;
}
	
.cprow{
	margin-top:30px;
	height : 50px;
	line-height : 50px;
	padding-left:20px;
}

.cprow div{
	display:inline-block;
	padding-left:10px;
}

.controlpanel{
	text-align : right;
	margin-right:30px;
}

.controlpanel input{
	height : 40px;
	width :100px;
}

.action_bar .switch-wrapper{
	padding-right:30px;
}

.row.parent{
	background : #363437;
}

.row.parent .user{ font-weight:bold;}

.action_bar.viewonly{
	display:none;
}

.norecord{
	width:100%;
	text-align:center;
	padding : 30px 0;
}

.showmore{
	    color: white;
    border: 1px solid #FFFFFF;
    padding: 3px;
}


@media only screen and (max-device-width: 480px){
.container {padding: 20px 0;}	
.action_bar input{ width:65px;}
.account_list .row .user {width:400px!important;}
.account_list .row{min-height:70px;}
.container .userinfo{padding-left:20px;padding-top:20px;}
}

.account_list .row{
	cursor:pointer;
}

.user .stats{
	padding-left : 20px;
}

.expandable{
	margin-top : 10px;
}
</style>

<body>
<?php

	
	$conn_r = array("host" => $host, "ip" => $ip, "port" => $port);

	$json = array();
	$json["account"] = $g_userid;
	$json["action"] = "MARKET_DATA";
	$json["value"] = "TRUE";

	$connection = new Connection();
	$connection->setconnect($conn_r);
	$result =  $connection->sendcommand($json);	
	if ($result != ""){
		$market_data = json_decode($result);
	}
//	var_dump($market_data);

/*	class market_data()
	{
		public $vwap;	public $netQty;
		public $vwapDiff;	public $vwap;
		public $prevClose; public $high; public $low; 
		public $ppThreshold; public $ppDirection;
	}

	$market_data = new market_data();
	$market_data->vwap = 28300;
	$market_data->netQty = "";
	$market_data->vwap = 28300;*/

	 ?>

	 
	 
	 <div class="container">
	 
		<div class="userinfo"> Accounts : 
		<select id="acc_list" name="acc_list" style="width:200px;"> 
				<option value="<?php echo $g_userid; ?>"><?php echo $g_userid; ?></option>
<?php 			
			if ($obj){
				foreach ($obj as $userid => $userinfo)
				{
					if ($userid != $g_userid){
					?>
					<option value="<?php echo $userid; ?>"><?php echo $userid; ?></option>
			<?php	}
				}
			}
			?>				
			</select> <input type="button" id="cp_refresh"   name="Refresh" value="Refresh"  ></input>
			</div>
		

		<div class="account_list">
			<input type="hidden" name="market_close" value="-1" />
			<input type="hidden" id="tradeNominal_<?php echo $userid; ?>" name="tradeNominal" value="-1" />


<?php 	
	if ($obj){
	foreach ($obj as $userid => $userinfo)
	{
		
		//	var_dump($userinfo);
			
		// remote configuration			
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "REMOTE_CONFIGURATION";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					
					$remote_config = json_decode($result);
			//		var_dump($remote_config);
				//["forceSettle"]=> string(5) "false" ["breakPt"]=> string(2) "-1" ["reverse"]=> string(5) "false" ["forcePosition"]=> string(0) "" ["forceOpen"]=> string(5) "false" 		
				//	echo $result->forceOpen;
				/*	$remote_config->forceSettle;
					$remote_config->breakPt;
					$remote_config->reverse;
					$remote_config->forcePosition;				
					$remote_config->forceOpen;*/
				}

				
				
		// active order

			//	var_dump($json);
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ACTIVE_ORDER";
				$json["value"] = "TRUE";
				
				$active_order = array();
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$active_order = json_decode($result);
				//	var_dump($result);
					
				}
				
		//balance		
				
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "BALANCE";
				$json["value"] = "TRUE";
				

				$balance = 0;
				$connection = new Connection();			
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$balance = json_decode($result);
				//	var_dump($balance);
				}
				
				//var_dump($remote_config, $active_order, $balance);

		//algo status		
				
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ALGO_STATUS";
				$json["value"] = "TRUE";
				$json["sync"] = "FALSE";
				

				$connection = new Connection();			
				$connection->setconnect($conn_r);
				$algo_status =  trim($connection->sendcommand($json));	

		// order statistics

			//	var_dump($json);
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ORDER_STATISTICS";
				$json["value"] = "TRUE";
				$json["sync"] = "FALSE";
				
				$order_stats = array();
				$buycount = 0;
				$sellcount = 0;
				$productCde = "-";
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				
				if ($result != "")
				{
					$order_stats = json_decode($result);
					$order_stats = json_decode($order_stats);
					//var_dump($result ,$order_stats);
					if ($order_stats){
					foreach ($order_stats as $stats){
						$buycount += $stats->buyCnt;
						$sellcount += $stats->sellCnt;
						$productCde = $stats->prodCde;
					}
					}
				}		
			
		
	//	if ($userid == "shadowTest" ){$userinfo->viewonly = 1; }
?>
		<form method="POST" action="javascript:void(0);" >
		<div class="row <?php echo $userid == $g_userid ? "parent" : ""; ?>">
			<?php
			

				//echo $algo_status;
				$status_title = "-";
				$status_color = "";
				$status_algo = "";
				if ($algo_status == "EMPTY" || $algo_status == "0"){
					$status_title =  "Ready";
					$status_color = "green";
				}
				if ($algo_status == "EXECUTING" ){
					if ($active_order){
						$netprice = $market_data->tradeNominal - $active_order->commitAvgPrice; // LONG
						if ($active_order->position == "SHORT"){
							$netprice = $netprice * -1;
						}
						
						$netprice = $netprice > 0 ? "+".$netprice : $netprice;
						$status_title =  $active_order->position." ".$active_order->commitAvgPrice." <b>(".$netprice.")</b>";
						$status_color = ($netprice > 0 ? "green" : "red");
					}else{
						$status_title =  "Ready";
						$status_color = "green";
					}
					
				}else if ($algo_status == "END"){
					$status_title =  "Game Over";
					$status_color = "red";
				}else{
					$status_title =  "-";
				}
				
				// warning for failed stats
				if (($algo_status != "EXECUTING" && $algo_status != "END") && $buycount != $sellcount){
					$status_title = "***WARNING***<br>".$status_title;
					$status_color = "red";
				}
				
				// total text
				$total = 0;
				$price_value = 10;
				
				if (intval($balance) != 0){
					if ($active_order && $active_order->marketDataKey->product == "HSI"){
						$price_value = 50;
					}
					$total = $balance * $price_value;
				}
				$totaltext = "$".number_format($total, 2);
				if ($total < 0){
					$totaltext = "(".$totaltext.")";
				}
			?>
		
			<input type="hidden" name="price_value" value="<?php echo $price_value;?>">
			<input type="hidden" name="userid" value="<?php echo $userid; ?>" />
 			<input type="hidden" name="server" value="<?php echo $userinfo->server; ?>"/>
			<input type="hidden" name="port" value="<?php echo $userinfo->port; ?>"/>
			<input type="hidden" name="sync" value="0">
			<input type="hidden" name="algo_status" value="<?php echo $algo_status; ?>">
			<input type="hidden" name="active_order" value="0">
			<input type="hidden" name="order_stats" value="0">
			<input type="hidden" name="buyCnt" value="<?php echo $buycount;?>">
			<input type="hidden" name="sellCnt" value="<?php echo $sellcount;?>">
			<input type="hidden" name="productCde" value="<?php echo $productCde;?>">


			<input type="hidden" name="viewonly" value="<?php echo $userinfo->viewonly == 1 ? "1" : "0"; ?>">
			
			<div class="user">
			<?php if ($userinfo->viewonly != 1) { ?>
			<input type="checkbox" name="acc_check"  <?php echo $userinfo->viewonly == 1 ? "disabled" : ""; ?> />
			<?php } ?>

			
			<?php echo $userid;?>
			<input type="checkbox" name="acc_sound"/>
			
			<?php 
			if (is_array($order_stats) && sizeof($order_stats) > 0){ ?>
			<span class="order_stats"> <?php echo $productCde . " | Buy : ". $buycount. " | Sell : ". $sellcount; ?></span>
			<?php } ?>

			</div>
			
			<div class="trade_status <?php echo $status_color; ?>">
				<?php echo $status_title; ?>
			</div>
			<div id="<?php echo $userid;?>loading" class="loading"></div>
			<div class="point <?php echo $balance > 0 ? "green" : ($balance < 0 ? "red" : ""); ?>">
			<?php echo $balance > 0 ? "+".$balance : $balance; ?>
			
			</div>
			<div class="balance  <?php echo $balance > 0 ? "green" : ($balance < 0 ? "red" : ""); ?>">
			<?php 
				
				
				echo $totaltext;
			?>
			</div>
			<div class="expandable" > <a href="#" class="showmore" >▼</a></div>
			<div class="action_bar clickable <?php echo $userinfo->viewonly == "1" ? "viewonly" : ""; ?>">

				<div class="switch-wrapper">
					<?php 
						$algo_on = 0;
						if ($remote_config){
							if ($remote_config->forcePosition == "LONG" || $remote_config->forcePosition == "SHORT"){
								$algo_on = 1;
							}else if ($remote_config->forcePosition == "NIL"){
								$algo_on = 0;
							}else{
								$algo_on = 1;
							}
						}
					
					?>
				
					<input type="checkbox" name="btnalgo" value="1" <?php echo $algo_on ? "checked" : ""; ?> >
				</div>	
				
				
		  
				<input type="button" class="btn btncall <?php echo $status_algo == "call" ? "on" : ""; ?>" name="Call" value="Call" ></input>
				
				
				<input type="button"  class="btn btnsettle <?php echo $status_algo != "" ? "on" : ""; ?>" name="Settle" value="Settle" ></input>
				
				<input type="button" class="btn btnput <?php echo $status_algo == "put" ? "on" : ""; ?>" name="Put" value="Put" ></input>
				
			</div>	
			<?php if ($userinfo->viewonly == "1" ){ ?>
				<div class="grey">
					View ONLY
				</div>
			<?php } ?>
			
		</div>
		</form>
<?php 
	}  ?>
	
	<div class="cprow">
		All <input type="checkbox" class="cp_all"  />
		<div class="controlpanel">
			
		
			Sync 
			<input type="submit" id="cp_call" class="btn btncall" name="Call" value="Call" ></input>
				
				<input type="submit" id="cp_settle" class="btn btnsettle" name="Settle" value="Settle" ></input>
				
			<input type="submit" id="cp_put" class="btn btnput" name="Put" value="Put" ></input>
			
			
					
		</div>
		
		</div>
<?php	
	}else{
	?>
		<div class="norecord">Connection failed</div>
	
	<?php } ?>
		</div>
	
		
	</div>	
	<!--<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
	<script src="alertify/lib/alertify.min.js"></script>
	<script src="js/jquery.easy-overlay.js?v1.2"></script>

	<script src="js/jquery.session.js"></script>
    <script src="js/jquery.switchButton.js"></script>
	<script src="js/jquery.number.js"></script>
	
	<script src="js/jquery.cookie.js"></script>

    <script>
      $(function() {
		  
		var url = "OmniController.php"; 
		var market_close = 0;
		var refresh_rate = <?php echo refresh_rate(1); ?>;
		var refresh_rate_min = <?php echo refresh_rate(60); ?>;
		var refresh_rate_balance = <?php echo refresh_rate(2); ?>;
		var isRemoteConfigReady = true; 
		var isAlgoStatusReady  = true; 
		var isActiveOrderReady = true; 
		var isBalanceReady = true; 
		var isActiveOrderReadyMap = new Map();
		var isAlgoStatusReadyMap = new Map();
		var isRemoteConfigReadyMap = new Map();
		checkAlgoOn(); 
		  
		setInterval(function () {refreshAccountList()}, 50);//request every x seconds
		
		setInterval(function () {refreshOrderStatsProc()}, refresh_rate_min);//request every x seconds
		
		//setInterval(function () {refreshBalanceList(false)}, refresh_rate_balance);//request every x seconds

		var soundclip = 'assets/ragnarok.mp3';

		var audio = new Audio(soundclip);
		function playsound() {
			/* some code goes here*/
			//27 Mar: veeko tested not necessary and remove
			//if(audio.paused) {audio.currentTime=0;audio.play()}
			//else  audio.pause();
			audio.play();
		}
		
	//	unlock();
		playsound();
	
		  
         $(".switch-wrapper input").switchButton({
          width: 50,
          height: 20,
		  button_width: 20
        });
		
		
		$(".showmore").click(function(){
			$(this).parent().parent().find(".clickable").toggle();
		});
		
		function checkAlgoOn(){
			$(".switch-wrapper input[name=btnalgo]").each(function(){  
				var checked = true;
				if ($(this).is(':checked')){
					$(this).parent().find(".switch-button-background").css("background-color", "#3ae428");
					checked = true;
				}else{
					$(this).parent().find(".switch-button-background").css("background-color", "#ccc");
					checked = false;
				}
				 $(this).switchButton({
					  width: 50,
					  height: 20,
					  button_width: 20,
					  checked : checked
					});
				
				
			});
			
		}
		
			
		$(".switch-wrapper input[name=btnalgo]").change(function(){
			
			
			var userid = $(this).parent().closest(".row").find("input[name='userid']").val();
			var port = $(this).parent().closest(".row").find("input[name='port']").val();
			var server = $(this).parent().closest(".row").find("input[name='server']").val();
				// console.log("BtnAlgo on/off=" + $(this).is(":checked"));
			
			var data = {};			
			data.host = server;
			data.port = port;
			data.userid = userid;
			data.value = "NIL";
			data.sync = false;
			
			if ($(this).is(':checked')){			
				// reset algo
				data.action = "reset";
				resultmsg = "Algo on id = "+userid;
				$(this).parent().find(".switch-button-background").css("background-color", "#3ae428");
			}else{								
				// off algo
				$(this).parent().find(".switch-button-background").css("background-color", "#ccc");
				data.action = "force_position";
				resultmsg = "Algo off id = "+userid;
			}
			
			$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							// console.log(resultmsg);
							
						}
					}
			);
			
			checkAlgoOn();
		});
		
		$(".action_bar .btncall").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
			
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			
			// console.log(userid +" call");
			$("#"+userid+"loading").easyOverlay("start",{width:100,height:100});
			// active order?
				var data = {};
				data.action = "force_open";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "LONG";
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;

				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							//refreshAccountList();
							// console.log(json);
							
						}
						
						alertify.set("notifier","position", "top-left");
						//alertify.success("Order received!");
					}
				).always(function(){
					//$("#"+userid+"loading").easyOverlay("stop");
					//top.refreshOrderStat();
					orderCmdDoneAction(userid);
					
				});;
				
		});
		
		$(".action_bar .btnput").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			// console.log(userid +" put");
			
			// active order?
				var data = {};
				data.action = "force_open";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "SHORT";
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;
				$("#"+userid+"loading").easyOverlay("start",{width:100,height:100});
				// console.log(data);
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							//refreshAccountList();
							// console.log(json);
							
						}
						alertify.set("notifier","position", "top-left");
						//alertify.success("Order received!");
					}
				).always(function(){
					//$("#"+userid+"loading").easyOverlay("stop");
					//top.refreshOrderStat();
					orderCmdDoneAction(userid);
					
				});;
		});
		function orderCmdDoneAction(userid){
			//$("#"+userid+"loading").easyOverlay("stop");
			setTimeout(function(){refreshAccountInfo();},1000);
		}
		function refreshAccountInfo(){
			refreshStatusList();
			top.refreshOrderStat();
		}

		$("#cp_refresh").click(function(){
			
			refreshAccountInfo();
			
			//refreshStatusList();
			//top.refreshOrderStat();
			
			
			//refreshAccountList();
			//refreshBalanceList(true);
			
		});
		
		
		$(".action_bar .btnsettle").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			// console.log(userid +" settle");
			$("#"+userid+"loading").easyOverlay("start",{width:100,height:100});

			// active order?
				var data = {};
				data.action = "force_settle";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							//refreshAccountList();
							// console.log(json);
							
						}
						alertify.set("notifier","position", "top-left");
						//alertify.success("Order received!");
							
					}
				).always(function(){
					//$("#"+userid+"loading").easyOverlay("stop");
					//top.refreshOrderStat();
					orderCmdDoneAction(userid);
					
				});
		});
		
		function refreshRemoteConfig(row, userid, port, server){
				
				var data = {};
				data.action = "remote_configuration";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				isRemoteConfigReady=isRemoteConfigReadyMap.get(userid);
			    if(isRemoteConfigReady||isRemoteConfigReady==undefined){
					isRemoteConfigReady = false;
					isRemoteConfigReadyMap.set(userid,false);
					$.post(
						url,
						data,
						function(json) {
							// console.log("--refreshRemoteConfig("+userid+")--"+json.algo_on);
							if (json.ok == 1){
								var checked = (json.algo_on == 1) ? true : false;
							
								$(row).find(".switch-wrapper input[name='btnalgo']").prop("checked", checked);
								checkAlgoOn();
								
							}
						}
					).always(function(jqXHR, textStatus, errorThrown){	
						isRemoteConfigReady = true;
						isRemoteConfigReadyMap.set(userid,true);
					});
				}
		}
		
		function refreshAlgoStatus(row, userid, port, server){
			// algo status
				var data = {};
				data.action = "algo_status";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				data.sync = false;
				isAlgoStatusReady = isAlgoStatusReadyMap.get(userid);
				if(isAlgoStatusReady ||isAlgoStatusReady==undefined){
					isAlgoStatusReady = false;
					isAlgoStatusReadyMap.set(userid,false);
					$.post(
						url,
						data,
						function(json) {
							// console.log(json);
								
							if (json.ok == 1){
							
								var prevStatus = $(row).find("input[name='algo_status']").val();
								if(prevStatus=="EXECUTING" && json.status=="READY"){
									refreshBalanceList(true);
								}
								if(prevStatus!=json.status){
									$("#"+userid+"loading").easyOverlay("stop");	//when status change then stop loading icon
								}
								$(row).find("input[name='algo_status']").val(json.status);
								refreshBalanceList(true)
								
							}
					
						}
					).always(function(jqXHR, textStatus, errorThrown){	
							isAlgoStatusReady = true;
							isAlgoStatusReadyMap.set(userid,true);
					});
				}

		}
		
		function refreshActiveOrder(row, userid, port, server){
			// active order?
					
						
						if(true){
					var data = {};
					data.action = "active_order";
					data.host = server;
					data.port = port;
					data.userid = userid;
					data.value = true;
					
									 //echo $algo_status;
					var status_title = "-";
					var status_color = "";
				
			
					algo_status = $(row).find("input[name='algo_status']").val().trim();
					market_close = $(".account_list input[name=market_close]").val();
					algo_status = $(row).find("input[name='algo_status']").val().trim();
					
					if (algo_status == "EMPTY" || algo_status == "0"){
							status_title =  "Ready";
							status_color = "green";
							$.cookie('tradeStatus_'+userid, '0');
					}else if (algo_status == "END"){
								status_title =  "Game Over";
								status_color = "red";
							$.cookie('tradeStatus_'+userid, '0');
					}
					var tradeStatus = $(row).find(".trade_status").html();
					var list = tradeStatus.split(" ");
					if(algo_status != "EXECUTING"){
						$(row).find(".trade_status").html(status_title);
					}else if(list.length>=2){
						
						var position = list[0];
						var commitAvgPrice = list[1];
						var netprice = market_close - commitAvgPrice; // LONG
							if (position == "SHORT"){
								netprice  = netprice*-1;
							}
							//27 Mar: veeko bug fix for wrong selector
							//var sound_check = $(this).find(".user input[name=acc_sound]").prop("checked");
							var sound_check = $(row).find(".user input[name=acc_sound]").prop("checked");
							//if (userid == "rocketLive" || userid == "rocketDemo"){
								if (!sound_check ){
									playsound();
								}
							//}

							netprice = netprice > 0 ? "+" + netprice : netprice;
							status_title =  position + " " + commitAvgPrice+ " <b>(" +netprice+")</b>";
							// console.log(status_title);
							status_color = (netprice > 0 ? "green" : "red");
							$(row).find(".trade_status").html(status_title);
					}
					if (status_color == "red"){
						$(row).find(".trade_status").removeClass("green");
						$(row).find(".trade_status").addClass("red");
					}else{
						$(row).find(".trade_status").removeClass("red");
						$(row).find(".trade_status").addClass("green");
					}
					
					// console.log("??"+userid + " : "+algo_status);
						}
				    isActiveOrderReady = isActiveOrderReadyMap.get(userid);
					if(isActiveOrderReady||isActiveOrderReady==undefined){
						isActiveOrderReady = false;
						isActiveOrderReadyMap.set(userid,false);
					$.post(
						url,
						data,
						function(json) {
							
							status_title = $(row).find(".trade_status").text();
							if (json.ok == 1){
								
								algo_status = $(row).find("input[name='algo_status']").val().trim();
								
								market_close = $(".account_list input[name=market_close]").val();
								tradeNominal = $("#tradeNominal_"+userid).val();
								if (json.product == "HSI"){
									$(row).find("input[name='price_value']").val(50);
								}else{
									$(row).find("input[name='price_value']").val(10);					
								}
									
					// console.log(json);
				
					if (algo_status == "EMPTY" ){
								status_title =  "Ready";
								status_color = "green";
								$(".user input[name=acc_sound]").prop("checked", false);
								
						}
							
					if (algo_status == "EXECUTING"  ){
						if (json.position != "NIL" && json.position != "undefined"){
							var netprice = market_close - json.commitAvgPrice; // LONG
							if (json.position == "SHORT"){
								netprice  = netprice*-1;
							}
							//27 Mar: veeko bug fix for wrong selector
							//var sound_check = $(this).find(".user input[name=acc_sound]").prop("checked");
							var sound_check = $(row).find(".user input[name=acc_sound]").prop("checked");
							//if (userid == "rocketLive" || userid == "rocketDemo"){
								if (!sound_check ){
									playsound();
								}
							//}

							netprice = netprice > 0 ? "+" + netprice : netprice;
							status_title =  json.position + " " + json.commitAvgPrice+ " <b>(" +netprice+")</b>";
							// console.log(status_title);
							status_color = (netprice > 0 ? "green" : "red");
							//status_algo =  ($active_order->position == "LONG" ? "call" : "put");
							$.cookie('tradeStatus_'+userid, '1');
						}else{
							status_title =  "Ready";
							status_color = "green";
							$.cookie('tradeStatus_'+userid, '0');
						}
						
						
					}else if (algo_status == "END"){
						status_title =  "Game Over";
						status_color = "red";
						$.cookie('tradeStatus_'+userid, '0');
					}
					
					var buycount = $(row).find("input[name=buyCnt]").val();
					var sellcount = $(row).find("input[name=sellCnt]").val();
					// console.log(algo_status + "|"+buycount+"|"+sellcount);
					if ((algo_status != "EXECUTING" && algo_status != "END") && buycount != sellcount){
						status_title = "***WARNING***<br>".$status_title;
						status_color = "red";
						alertify.error("***WARNING***<br>Outstanding Order");
					}
					
					
					$(row).find(".trade_status").html(status_title);
					if (status_color == "red"){
						$(row).find(".trade_status").removeClass("green");
						$(row).find(".trade_status").addClass("red");
					}else{
						$(row).find(".trade_status").removeClass("red");
						$(row).find(".trade_status").addClass("green");
					}
							}
						
					// console.log(userid + " : "+status_title);
							
								
						}
					 ).fail( function(jqXHR, textStatus, errorThrown) {
						// console.log(textStatus);
					}).always( function(jqXHR, textStatus, errorThrown) {
						isActiveOrderReady = true;
						isActiveOrderReadyMap.set(userid,true);
					});
					
				}
				 
			
							

					
		}
		
		
		
		function refreshBalance(row, userid, port, server){
				// balance
				var data = {};
				data.action = "balance";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				price_value = $(row).find("input[name='price_value']").val();
				if(isBalanceReady){
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							var balance = json.balance;
							if (balance > 0){
								balance = "+"+balance;
								$(row).find(".point").removeClass("red");
								$(row).find(".point").addClass("green");
								
								$(row).find(".balance").removeClass("red");
								$(row).find(".balance").addClass("green");
																
							}else{
								$(row).find(".point").removeClass("green");
								$(row).find(".point").addClass("red");
																
								$(row).find(".balance").removeClass("green");
								$(row).find(".balance").addClass("red");
							}
							$(row).find(".point").text(balance); 
							
							var total = balance * price_value;
							var totaltext = "$" + $.number(total,2);
							if (total < 0){
								totaltext = "(" + totaltext + ")";
							}
							$(row).find(".balance").text(totaltext); 
						//	// console.log(json);
						}
					}
				).always( function(jqXHR, textStatus, errorThrown) {
					isBalanceReady = true;
				});
			
			}
		}
		
		
		function refreshOrderStatsProc(){
			
			market_close = $(".account_list input[name=market_close]").val();
				
			$(".row").each(function(){
				
				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				
				var row = $(this).parent();
			
				
				refreshOrderStats(row, userid, port, server);
				
					
				saveRecordAccount(row, userid, port, server);
			
				
			});
			
		}
		
		function refreshOrderStats(row, userid, port, server){
				// balance
				var data = {};
				data.action = "order_statistics";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				order_stats = $(row).find("input[name='order_stats']").val();
				
				$.post(
					url,
					data,
					function(json) {
						
						
							$(row).find(".order_stats").text(json.ordertext); 
						//	// console.log(json);
						
					}
				);
		}
		
		function refreshStatus(row, userid, port, server){
				var data = {};
				data.action = "REFRESH_STATUS";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				$.post(
					url,
					data,
					function(json) {
							//alert(json);
					}
				);
		}
		
		function saveRecordAccount(row, userid, port, server)
		{
				// balance
				var data = {};
				data.action = "save_record";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				$.post(
					url,
					data,
					function(json) {
						
						
							//$(row).find(".order_stats").text(json.ordertext); 
						//	// console.log(json);
						
					}
				);
		}
		
		function refreshAccountList(){
			
			var market_close = $.session.get('market_close');
			$(".account_list input[name=market_close]").val(market_close);
			
		//	// console.log("refreshAccountList :  market_close"+ market_close);
			
		
				
			$(".row").each(function(){
				

				
				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				var row = this;

				//// console.log(userid, port, server);
				
				refreshRemoteConfig(row, userid, port, server);
				//if(false){
				refreshAlgoStatus(row, userid, port, server);
				refreshActiveOrder(row, userid, port, server);
				//refreshBalance(row, userid, port, server);
				//}
				
				
			});
			
		}
		
		function refreshBalanceList(forceUpd){
			
			market_close = $(".account_list input[name=market_close]").val();
				
			$(".row").each(function(){
				

				
				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				var tradeStatus = $(this).find(".trade_status").text();
				//alert(tradeStatus);
				var row = this;
				
				if(forceUpd||tradeStatus.includes("LONG") || tradeStatus.includes("SHORT")){
					//alert(tradeStatus);
					refreshBalance(row, userid, port, server);
					//refreshActiveOrder(row, userid, port, server);
					
					
				}else if(tradeStatus.trim()=="-"||false&&tradeStatus.trim()=="Ready"){
					//refreshAlgoStatus(row, userid, port, server);
					//refreshActiveOrder(row, userid, port, server);
					
					
				}
				
				
			});
			
		}
		
		function refreshStatusList(){
			
	
			$(".row").each(function(){

				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				var tradeStatus = $(this).find(".trade_status").text();
				var row = this;
				refreshStatus(row, userid, port, server);	
			});
			
		}
		// Sync
		
		$(".cp_all").change(function(){
			// console.log($(this).is(":checked"));
			if ($(this).is(":checked")){
				$(".user input[name=acc_check]").prop("checked", true);
			}else{
				$(".user input[name=acc_check]").prop("checked", false);
			}
		});
		
		$("#cp_call").click(function(){
			
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
			
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btncall").click();
				}
			});
		});
		
		$("#cp_put").click(function(){
			
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btnput").click();
				}
			});

		});
		
		$("#cp_settle").click(function(){
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btnsettle").click();
				}
			});

		});
		
      })
	 
   
	
	
	</script>
	
  </body>
</html>