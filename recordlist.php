<?php

ini_set("allow_url_fopen", 1);
include_once("config.php");
include_once("connection.php");


$g_userid = "";
$host = "";
$ip = "";
$port = "";
if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}


?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#343235;
	color:#FFF;
	max-width :470px;
	overflow-x:hidden;
}

.container{
	width:1000px;
}

.optfilter ul li{
	display :inline;
	padding-left:30px;
}

.recordtable{
	background:#5f5d60;
	width:100%;
	height:270px;
}

.recordtable table{
	width:100%;
}

.recordtable table thead th{
	color : #FFF;
	width:13%;
	padding:3px;
	background:#1e1b20;
	font-size:12px;
	
}
.recordtable tr td{
	padding : 10px;
	border-bottom : #CCC solid 1px;
}

.textfield{
	color:#000000;
	width: 80px;
}

.textfield:disabled{
	background-color : #5f5d60;
	color:#FFF;
	width: 80px;
}

.save, .addprofile, .delprofile{
	display:none;
}

</style>
  <body>
 
 <div id="recordform">
 
	<input type="hidden" name="userid" value="<?php echo $g_userid; ?>" />
	<input type="hidden" name="server" value="<?php echo $host; ?>" />
	<input type="hidden" name="port" value="<?php echo $port; ?>" />	
	<input type="hidden" name="market_close" value="-1" />

	<div class="container">
		<div class="title">Record</div>
		<div class="optfilter">
		<ul>
			<li>All</li>
			<li>Working</li>
			<li>Filled</li>
			<li>Cancelled</li>
		</ul>
		</div>
		<div class="recordtable">
			 <table cellpadding="0" cellspacing="0" border="0">
			  <thead>
				<th>Code</th>
				<th>Price</th>

				<th style="width:400px;">Profile</th>
				<th style="width:120px;">Cut Gain</th>
				<th style="width:120px;">Cut Loss</th>
				<th style="width:120px;">Threshold</th>
				<th>Action</th>

			</thead>
			 <tbody>
			 <?php 
			 	
			$conn_r = array(
								"host" => $host,
								"ip" => gethostbyname($host),
								"port" =>$port
							);
			

			 
			 	$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "OPEN_ORDER";
				$json["value"] = "TRUE";
				
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$open_record = json_decode($result);
				}
				
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "PENDING_ORDER";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$pending_record = json_decode($result);
				}
				
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "SETTLED_ORDER";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$settled_record = json_decode($result);
				}
				
			

				
				if ($open_record){
				foreach ($open_record as $rec){ 
					$recordinfo = $rec->orderList[0];
					 ?>
					<tr>
						<?php
							$price = $recordinfo->commitAvgPrice;
							if ($price == 0){
								$price = $recordinfo->orderPrice;
							}
							
							
						?>
						<td><?php echo $recordinfo->id; ?> <br/> <?php echo date("Y-m-d H:i", $recordinfo->orderTime/ 1000); ?><input type="hidden" name="orderId" id="orderId" value="<?php echo $recordinfo->id; ?>"/></td>
						<td><?php echo $recordinfo->position; ?> <?php echo $price; ?></td>
						<td> 
						<select name="profile" class="profile_dropdown" data="<?php echo $recordinfo->id; ?>" onchange="changeAction(this)">
							<option value="0,0,0">--default-- (0/0/0)</option>
							<option value="30,30,90" >Profile1 (30/30/90)</option>
						</select>
						<button name="addprofile" class="addprofile" onclick="changeAction(this)">Add</button>
						<button name="delprofile" class="delprofile" onclick="changeAction(this)">Del</button>
						</td>
						<td><input type="text" class="textfield" id="maxprofit_<?php echo $recordinfo->id; ?>" name="maxprofit" value="<?php echo $recordinfo->profitThreshold; ?>" disabled /></td>
						<td><input type="text" class="textfield" id="maxloss_<?php echo $recordinfo->id; ?>"  name="maxLoss" value="<?php echo $recordinfo->lossThreshold; ?>" disabled /></td>
						<td><input type="text" class="textfield" id="trailingStop_<?php echo $recordinfo->id; ?>"  name="trailingStop" value="<?php echo $recordinfo->trailingStopRatio; ?>" disabled /></td>
						<td> Open <button name="edit" class="edit" onclick="changeAction(this)">Edit</button>  <button name="save" class="save" onclick="changeAction(this)">Save</button>  <input type="hidden" name="tradeStatus[]" class="tradeStatus" value="1" /></td>
					</tr>	
					
	<?php		}
			   }
			   
				if ($settled_record){
					
					//reverse settle record
					$settled_record = array_reverse($settled_record);
					
				foreach ($settled_record as $rec){ 
					$recordinfo = $rec->orderList[0];
				?>
					<tr>
						<?php
							$price = $recordinfo->commitAvgPrice;
							if ($price == 0){
								$price = $recordinfo->orderPrice;
							}
						?>
						<td><?php echo $recordinfo->id; ?> <br/> <?php echo date("Y-m-d H:i", $recordinfo->orderTime/ 1000 ); ?></td>
						<td><?php echo $recordinfo->position; ?> <?php echo $price; ?></td>
						<td></td>
						<td><?php echo $recordinfo->profitThreshold; ?></td>
						<td><?php echo $recordinfo->lossThreshold; ?></td>
						<td><?php echo $recordinfo->trailingStopRatio; ?></td>
						<td> Settled <input type="hidden" name="tradeStatus[]" class="tradeStatus" value="0" /></td>
					</tr>	
					
	<?php		}
			   }
			 
			
			 ?>
			 
			 
			 </tbody>
			 </table>
		
		</div>
	
	
	
	</div>
</div>	
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
	<script src="js/jquery.cookie.js"></script>
	
    <script>
	var url = "OmniController.php";
  	var market_close = 0;
	var refresh_rate = <?php echo refresh_rate(2); ?>;
	var userid = $("#recordform").find("input[name='userid']").val();
	var port = $("#recordform").find("input[name='port']").val();
	var server = $("#recordform").find("input[name='server']").val();
		 
	setInterval(function () {refreshOrderStat()}, refresh_rate);//request every x seconds

	$( document ).ready(function() {
		loadProfile();
	});
	function refreshOrderStat(){
		
		var tradeOpen = 0;
		$(".tradeStatus").each(function(k,v){
			console.log(k, v );
			if ($(v).val() == 1){
				tradeOpen = 1;
			}
		});
		
		console.log("------TRADING STATUS ======= " + $.cookie('tradeStatus_'+userid) + "|"+ tradeOpen);
		if ($.cookie('tradeStatus_'+userid) != null && $.cookie('tradeStatus_'+userid) != tradeOpen)
		{ 	
				//Refresh the current page.
				window.location.reload(false);
		}
		
			
	}
	
	function changeAction(obj){
			switch (obj.name){
				case "profile":
					var orderId = $(obj).attr("data");
					var ordersetting = obj.value.split(",");
					console.log(ordersetting);
					$("#maxprofit_"+orderId).val(ordersetting[0]);
					$("#maxloss_"+orderId).val(ordersetting[1]);
					$("#trailingStop_"+orderId).val(ordersetting[2]);
					$(".edit").hide();
					$(".save").show();
					$(".addprofile").show();
					$(".delprofile").show();
					$(".textfield").prop( "disabled", false );
					break;
				case "edit":
					$(".edit").hide();
					$(".save").show();
					$(".addprofile").show();
					$(".textfield").prop( "disabled", false );
					break;
					
				case "save":					 
					 var data = {};
					 data.action = "order_setting";
					 data.host = server;
					 data.port = port;
					 data.userid = userid;
					 data.orderId = $("#orderId").val();
					 data.maxProfit = parseInt($("#maxprofit_"+data.orderId).val());
					 data.maxLoss = parseInt($("#maxloss_"+data.orderId).val());
					 data.trailingStop = parseInt($("#trailingStop_"+data.orderId).val());
					 
					 console.log("save_setting");
					 console.log(data);
				
					 $.post(
						url,
						data,
						function(json){
								$(".save").hide();
								$(".addprofile").hide();
								$(".delprofile").hide();
								$(".edit").show();
								$(".textfield").prop( "disabled", true );
								
						
						}
					);
				
				break;
				case 'addprofile':
					var profileList = [];
					var storeprofile = $.cookie('tradeProfile_'+userid);
					
					if (storeprofile != null){
						profileList = $.parseJSON(storeprofile);
					}else{
				
						profileList = [
							{"name" : "--default-- (0/0/0)" , "value" : "0,0,0"},
							{"name" : "Profile1 (30/30/90)", "value" : "30,30,90"}
						];
					}
					// add new profile
					var orderId = $("#orderId").val();
					var newprofile=[
						parseInt($("#maxprofit_"+orderId).val()),
						parseInt($("#maxloss_"+orderId).val()),
						parseInt($("#trailingStop_"+orderId).val())
					];
					profileList.push({"name": "Profile"+profileList.length+" ("+ newprofile.join("/")+")", "value" : newprofile.join(",")});
					
					// aave profile
					$.cookie('tradeProfile_'+userid, JSON.stringify(profileList));
					
					// refresh profile select
					$(".profile_dropdown").empty();
					$.each(profileList, function(key,option) {
					  $(".profile_dropdown").append($("<option></option>")
						 .attr("value", option.value).text(option.name));
					});
					
					
				break;
				case 'delprofile':
					var profileName = $(".profile_dropdown option:selected").text();
					deleteProfile(profileName);
					loadProfile();
						
				break;
			}
		}
		function loadProfile(){
			var profileList = [];
			var storeprofile = $.cookie('tradeProfile_'+userid);
			
			if (storeprofile != null){
				profileList = $.parseJSON(storeprofile);
			}else{
			
				profileList = [
					{"name" : "--default-- (0/0/0)" , "value" : "0,0,0"},
					{"name" : "Profile1 (30/30/90)", "value" : "30,30,90"}
				];
			}

			// save profile
			$.cookie('tradeProfile_'+userid, JSON.stringify(profileList));
			
			// refresh profile select
			$(".profile_dropdown").empty();
			$.each(profileList, function(key,option) {
			  $(".profile_dropdown").append($("<option></option>")
				 .attr("value", option.value).text(option.name));
			});
			
		}
		
		function deleteProfile(profileName){
			var profileList = [];
			var storeprofile = $.cookie('tradeProfile_'+userid);
			
			if (storeprofile != null){
				profileList = $.parseJSON(storeprofile);
				for (var i = profileList.length; i--;) {
					if (profileList[i].name == profileName) {
						profileList.splice(i, 1);
						break;
					}
				}	
			}
			// save profile
			$.cookie('tradeProfile_'+userid, JSON.stringify(profileList));
			
		}
	
	 </script> 
  </body>
</html>