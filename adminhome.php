
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniTrader WebApp Admin Home</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	 <link rel="stylesheet" href="./css/admin.css?v=<?php echo date("YmdHis"); ?>">
<link rel="stylesheet" href="alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />

	 
</head>

<style>
iframe {max-height:400px;}
a {color:white;}
</style>

<body>

<iframe id="rocketLive" width="100%" height="50%" frameborder="0" src="admin.php?userid=rocketLive&host=10.26.0.50&port=7895&noheader=1" ></iframe>
<iframe id="rocketDemo" width="100%" height="50%" frameborder="0" src="admin.php?userid=rocketDemo&host=majestic2017.ddns.net&port=7895&noheader=1"></iframe>


<a href="./home.php?userid=rocketLive&host=10.26.0.50&port=7895&nopanel=1" target="_blank">Open OmniTrader</a>

</body>
</html>