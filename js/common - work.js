
	 	var url = "OmniController.php";
		var vwap_scale_max = 3000;

		function refreshMarketData(server, isauto){
				//check current time and date
				if (!$("input[name=autoExecVwap]").prop("checked")){
					return;
				}
						
				var data = {};
				data.action = "reload_mkt_data";
				data.host = server.host;
				data.ip = server.ip;
				data.port = server.port;
				data.userid = server.userid;
				data.value = "TRUE";
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							var time = new Date();
							alertify.success("["+time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds()+ "] Market data reloaded");
							console.log("Market data reloaded");
//							alert("");

							if (isauto == 1){
								$("#autoreload_time").html("[" + time.getHours() + ":"+ time.getMinutes ()+ ":"+ time.getSeconds() 
								+"] reloaded");
							}
						}
					});
					
				
		}



		function getMarketData(caller, server, is_refresh_marketdata, accframe, recframe) {
				
				console.log("+==== getMarketData("+caller + ") ====+" );

			 
		 

				console.log(server);
				
				var data = {};
				data.action = "market_data";
				data.host = server.host;
				data.ip = server.ip;
				data.port = server.port;
				data.userid = server.userid;
				$.post(
					url,
					data,
					function(json) {
						
					console.log("LastUpdate = " + json.lastupdate);
						if (json.ok == 1){
						
							var price_net = json.close_net;
							if (json.close_net > 0){
								$("#marketheader .current").removeClass("red");
								$("#marketheader .current").addClass("green");
								price_net = "+"+json.close_net;
							}else{		
								$("#marketheader .current").removeClass("green");
								$("#marketheader .current").addClass("red");
							}
							$("#marketheader .netQty").text(json.netQty);
							$("#marketheader .market_price").text(json.close);
							$("#marketheader .market_price_net").text(price_net);
							
							$("#marketheader .market_info .high span").text(json.high);
							$("#marketheader .market_info .low span").text(json.low);
							$("#marketheader .market_info .ppThreshold span").text(json.ppThreshold);
							$("#marketheader .market_info .ppThreshold b").text(json.ppDirection);
							$("#marketheader .market_info .open span").text(json.open);
							
							var vdiff = json.vwapDiff;
							if (vdiff > 0){vdiff = "+"+vdiff};
							$("#marketheader .vwap span").text(json.vwap+" "+vdiff);
							//$("#marketheader .vwapvalue").val(parseInt(json.vwap));
							//veeko added
							$("#marketheader .vwapvalue").val(json.vwapraw);
							$("#marketheader #last_updated").text(json.lastupdate);
							
							// 2019-06-19
							$("#tradenorm").text(json.tradeNominal);
							$("#tradepro").text(json.tradeproduct);
							
							console.log("Last update time : Elapsed Time = "+ json.elapsed_time + "| current time = "+json.now+" | diff="+json.diff);							
							if (json.elapsed_time > 0){
								
								if (json.elapsed_time < elapsed_time_max){
								
									var objDate = new Date();
									var hours = objDate.getHours();
									var weekday = objDate.getDay();
									//if ((hours >= 9 && hours <= 12) || (hours >= 13 && hours <= 17)){
									if ((hours >= 9 && hours < 12) || (hours >= 13 && hours <= 23)){	//testing tobe remove
										
										if (weekday >= 1 && weekday <= 5){
									
											alertify.error("Warning : Last Update Timer Stopped <br>("+json.elapsed_time +"min)");
											if (is_refresh_marketdata){
												refreshMarketData(1);
											}
										}
									}else{
											console.log("Warning : Last Update Timer Stopped <br>("+json.elapsed_time +"min)");
									}
									
								}else{
									$("#autoreload_time").html("["+json.elapsed_time+" min] Last Update Timer Stopped");
									
								}	
							}
							
							showVwapGauge();
							
							$(recframe).find('input[name=market_close]').val(json.close);
							$(recframe).find('input[name=tradeNominal]').val(json.tradeNominal);
							$(accframe).find('input[name=market_close]').val(json.close);
							$(accframe).find('input[name=tradeNominal]').val(json.tradeNominal);
							//console.log($(accframe).find('.account_list input[name=market_close]').val());
																			
							return 1;
						}

				}).fail( function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
					//alert(textStatus);
				});
				
				
		}
		
		function vwapscale(value){
			
			
			var vwap = $("#marketheader .vwapvalue").val();
		
			value = parseInt((value - (vwap - 230)) * vwap_scale_max / 460);
				console.log("value= " + value + " | converted ="+ value);
			return value > 0 ? (value > vwap_scale_max ? vwap_scale_max : value) : 0;
			
		}
		
		
		function showVwapGauge(){
			
			
			var vwap = parseInt($("#marketheader .vwapvalue").val());
			var market_price = parseInt($("#marketheader .market_price").text());
			var ppThreshold = parseInt($("#marketheader .market_info .ppThreshold span").text());
			console.log(market_price);
			
			var interval = [vwap-230, vwap-200, vwap-100, vwap-2, vwap+2, vwap+100, vwap+200, vwap+230];
			var intcolor = ["#F03E3E", "#FFDD00", "#30B32D", "#0000FF",  "#30B32D", "#FFDD00", "#F03E3E"];
			var pindex = interval.length-1;
			for (var i=0; i < interval.length; i++){
				if (ppThreshold < interval[i]){
					if (i > 0){
						console.log(i);
						interval.splice(i,0,ppThreshold-2, ppThreshold+2);
						intcolor.splice(i,0, "#00deff", intcolor[i-1]);					
					}else{
						interval.splice(i,0, ppThreshold-2, ppThreshold+2);
						intcolor.splice(i,0, "#00deff", intcolor[i]);
					}
					pindex = i;
					break;
				}
			}
			if (pindex == interval.length-1){ // threshold last
				interval.push(ppThreshold-2);
				interval.push(ppThreshold+2);
				intcolor.push("#00deff");
				intcolor.push(intcolor[pindex-1]);
			}
			
			console.log(pindex);
			console.log(interval);
						console.log(intcolor);
			
			var zones = [];
			
			for (var i=0; i < interval.length; i++){
				var obj = {};
				obj.strokeStyle = intcolor[i];
				obj.min = vwapscale(interval[i]);
				obj.max = vwapscale(interval[i+1]);
				zones.push(obj);
			}
			/*
		   {strokeStyle: "#F03E3E", min: vwapscale(vwap-230), max: vwapscale(vwap-200)}, // Red from 100 to 130
		   {strokeStyle: "#FFDD00", min: vwapscale(vwap-200), max: vwapscale(vwap-100)}, // Yellow
		   {strokeStyle: "#30B32D", min: vwapscale(vwap-100), max: vwapscale(vwap-1)}, // Green
		   {strokeStyle: "#0000FF", min: vwapscale(vwap-1), max: vwapscale(vwap+1)}, // White				
		   {strokeStyle: "#30B32D", min: vwapscale(vwap+1), max: vwapscale(vwap+100)}, // Green
		   {strokeStyle: "#FFDD00", min: vwapscale(vwap+100), max: vwapscale(vwap+200)}, // Yellow
		   {strokeStyle: "#F03E3E", min: vwapscale(vwap+200), max: vwapscale(vwap+230)}  // Red
	
	*/
			
			var opts = {
			  angle: -0.04, // The span of the gauge arc
			  lineWidth: 0.2, // The line thickness
			  radiusScale: 0.83, // Relative radius
			  pointer: {
				length: 0.6, // // Relative to gauge radius
				strokeWidth: 0.046, // The thickness
				color: '#FFFFFF' // Fill color
			  },
			  limitMax: false,     // If false, max value increases automatically if value > maxValue
			  limitMin: false,     // If true, the min value of the gauge will be fixed
			
			  strokeColor: '#E0E0E0',  // to see which ones work best for you
			  generateGradient: true,
			  highDpiSupport: true,     // High resolution support
			  staticZones: zones,
			  staticLabels: {
				  font: "10px sans-serif",  // Specifies font
				  labels: [vwapscale(vwap-200), vwapscale(vwap-100), vwapscale(vwap+100), vwapscale(vwap+200)],  // Print labels at these values
				  title : [vwap-200, vwap-100, vwap+100, vwap+200],
				  color: "#FFFFFF",  // Optional: Label text color
				  fractionDigits: 0  // Optional: Numerical precision. 0=round off.
			  },
			};
			var target = document.getElementById('vwapgauge'); // your canvas element
			var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
			gauge.maxValue = vwap_scale_max; // set max gauge value
			gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
			gauge.animationSpeed = 1; // set animation speed (32 is default value)
			gauge.set(vwapscale(market_price)); // set actual value
		}

			
		 