<?php


$html=<<<HTMLTXT
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>
<body>
 <b style="color:#FFF;"> Notification off</b>
 </body>
 </html>

HTMLTXT;

echo $html;
exit;


ini_set("allow_url_fopen", 1);

include_once("config.php");
include_once("connection.php");
include_once("notification.class.php");

//var_dump($obj);
	
	/*
	ob_start();
for($i=0;$i<10;$i++) {
   echo str_repeat(" ",10000);
   echo 'printing...<br />';
   ob_flush();
   flush();
   sleep(1);
}*/

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#363437;
	color:#FFF;
	max-width :470px;
	overflow:scroll;
}

.nlist .row{
	font-size : 12px;
	min-height : 60px;
	padding:15px;
}

.nlist .row:nth-child(odd) {background: #5f5d60}
.nlist .row:nth-child(even) {background: #000}
.info_filter ul{margin:0; padding:0;}
.info_filter ul li{display : inline; padding-left:20px;}

.notitable { background-color:#363437;}
.notitable .title{padding:10px;}
.info_filter .selected {width:40px; background:#067eff; padding:3px;}
.info_filter {padding:10px;}
.info_filter .t2{padding-top:10px;}

</style>
  <body>
  <?php
  
	$notifyCtrl = new notificationController();
	$criteria = array();
	$criteria["date_fr"] = date("Y-m-d");
	$criteria["date_to"] = date("Y-m-d", strtotime($criteria["date_fr"]." +1 day"));
	$result = $notifyCtrl->loadData($criteria);
	
	$notification = array();
	foreach ($result as $res){
		$statement = "[".$res["time"]."] ".$res["key"]."<br>";
		$statement .= $res["msg"];
		array_push($notification, $statement);
	}
	
	$category = $notifyCtrl->loadCategory();
	
	//echo "<pre>";
	//var_dump($category);
	
  ?>
 
	<div class="notitable" >
		<div class="title">Wise Info </div>
		
			
		<div class="target">
		<div class="info_filter">
			<ul class="t1">
				<li class="selected"> 1 min </li>
				<li> 5 min </li>
				<li> 10 min </li>
				<li> 15 min </li>
			</ul>
			<ul class="t2">
	
				<li class="selected"> All </li>
				
			</ul>	
		</div>
		</div>	
		<div class="nlist">
		<?php 
		//var_dump($notification);
			foreach ($notification as $id => $ninfo){ ?>
			<div class="row">
				<?php echo $ninfo; ?>
			</div>
			
		<?php } ?>	
		</div>
	</div>
	
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
		<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>

    <script>
 
	 $(document).ready(function(){
		$('.select a').on('click',function(e){
			e.preventDefault();
			var aID = $(this).attr('href');
			var elem = $(''+aID).html();

			$('.target').html(elem);
		});
	});
 
	</script>
  </body>
</html>