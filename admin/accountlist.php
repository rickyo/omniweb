<?php

ini_set("allow_url_fopen", 1);
include_once("../config.php");
include_once("../connection.php");

if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}



//$url = "http://veeko123.ddns.net/majestic/omni/user_account.php?userid=".$g_userid;
$url = "http://localhost:8080/majestic/omni/user_account.php?userid=".$g_userid;

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);
$result = curl_exec($ch);
curl_close($ch);

$obj = json_decode($result);

if (_DEBUG){
	//$url = "http://localhost/majestic/omni/user_account.php?userid=".$g_userid;
	
	//$res = include("data.php");
	//$obj = json_decode($res, 1);
	
	
}

//echo $url;
//var_dump($obj);
	
	/*
	ob_start();
for($i=0;$i<10;$i++) {
   echo str_repeat(" ",10000);
   echo 'printing...<br />';
   ob_flush();
   flush();
   sleep(1);
}*/

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#FFF;
	color:#000;
	max-width :800px;
	overflow-x:hidden;
	overflow-y:scroll;
}

.green{
	color:#00CC00;
}

.red{
	color:#CC0000;
}

.grey{
	color :#808080;
}

.container{
	width:100%;
	max-width :1200px;
	padding:20px;
}

.align-left{
	float:left;
	width:40%;
}

.align-right{
	float:right;
	width:60%;
}

.clearfix{
	clear:both;
}

.market_price{
	font-size:26px;
	font-weight: bold;
}

.market_price_net{
	font-size:18px;
}

.market_info tr> td{
	font-size:16px;
}
.market_info .offset-left-30{
	padding-left:20px;
}

.header{
	padding-top:20px;
}

.account_list{
	margin-top:30px;
	border-top : 1px solid #CCCCCC;
}

.account_list .row{
	height : 70px;
	border-bottom : 1px solid #CCCCCC;
	padding:20px;
}



.account_list .row .user, .account_list .row  .trade_status, .account_list .row .point , .account_list .row .balance{
	
	display:inline-block;
}
.account_list .row .trade_status{
	width:200px ;
}
.account_list .row .point , .account_list .row .balance{
	width:80px;
}
.switch-wrapper {
  display: inline-block;
  position: relative;
  top: 3px;
  line-height:50px;
}

.switch-button-label {
    float: left;

    font-size: 10pt;
    cursor: pointer;
}

.switch-button-label.off {
    color: #adadad;
}

.switch-button-label.on {
    color: #0088CC;
}

.switch-button-background {
    float: left;
    position: relative;

    background: #ccc;
    border: 1px solid #aaa;

    margin: 1px 10px;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;

    cursor: pointer;
}

.switch-button-button {
    position: absolute;

    left: -1px;
    top : -1px;

    background: #FAFAFA;
    border: 1px solid #aaa;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

.btncall{
	background-color : transparent;
	border-radius:3px;
	height : 10px;
	width :70px;
	border : 3px solid #00CC00;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#00CC00;
}

.btncall.on{
	background-color : #00CC00;
	color:#FFF;
}

.btnput{
	background-color : transparent;
	border-radius:3px;
	height : 10px;
	width :70px;
	border : 3px solid #CC0000;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CC0000;
}

.btnput.on{
	background-color : #CC0000;
	color:#FFF;
}
.btnsettle{
	background-color : transparent;
	border-radius:3px;
	height : 10px;
	width :70px;
	border : 3px solid #007eff;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#007eff;
}
.btnsettle.on{
	background-color : #007eff;
	color:#FFF;
}
	
.cprow{
	margin-top:30px;
	height : 50px;
	line-height : 50px;
	padding-left:20px;
}

.cprow div{
	display:inline-block;
	padding-left:10px;
}

.controlpanel{
	text-align : right;
	margin-right:30px;
}

.controlpanel input{
	height : 40px;
	width :100px;
}

.action_bar {
	width:430px;
}

.action_bar .switch-wrapper{
	padding-right:30px;
}

.row.parent{
	background : #ECECEC;
}

.row.parent .user{ font-weight:bold;}

.action_bar.viewonly{
	display:none;
}

.norecord{
	width:100%;
	text-align:center;
	padding : 30px 0;
}


@media only screen and (max-device-width: 480px){
.container {padding: 20px 0;}	
.action_bar input{ width:65px;}
.account_list .row .user {width:400px!important;}
.account_list .row{height:70px;}
.container .userinfo{padding-left:20px;padding-top:20px;}
}

.account_list .row{
	cursor:pointer;
}

.user .stats{
	padding-left : 20px;
}

.account_list table{
	width:1200px;
}

.account_list table th{
	background : #bcbcbc;
	font-size:10px;
	line-height:30px;
}	

.account_list table tr>td{
	height : 50px;
	line-height:50px;
	border-bottom:1px solid #ECECEC;
}

</style>

<body>
<?php

	
	$conn_r = array("host" => $host, "ip" => $ip, "port" => $port);

	$json = array();
	$json["account"] = $g_userid;
	$json["action"] = "MARKET_DATA";
	$json["value"] = "TRUE";

	$connection = new Connection();
	$connection->setconnect($conn_r);
	$result =  $connection->sendcommand($json);	
	if ($result != ""){
		$market_data = json_decode($result);
	}
//	var_dump($market_data);

/*	class market_data()
	{
		public $vwap;	public $netQty;
		public $vwapDiff;	public $vwap;
		public $prevClose; public $high; public $low; 
		public $ppThreshold; public $ppDirection;
	}

	$market_data = new market_data();
	$market_data->vwap = 28300;
	$market_data->netQty = "";
	$market_data->vwap = 28300;*/

	 ?>

	
	 
	 <div class="container">
	 

			
			 	<div class="cprow">
		All <input type="checkbox" class="cp_all" checked />
		<div class="controlpanel">
			
		
			Sync 
			<input type="submit" id="cp_call" class="btncall" name="Call" value="Call" ></input>
				
				<input type="submit" id="cp_settle" class="btnsettle" name="Settle" value="Settle" ></input>
				
	<input type="submit" id="cp_put" class="btnput" name="Put" value="Put" ></input>
							
		</div>
		
				<div class="userinfo"> Accounts : 
			<select id="acc_list" name="acc_list" style="width:200px;"> 
				<option value="<?php echo $g_userid; ?>"><?php echo $g_userid; ?></option>
<?php 			
			if (sizeof($obj) > 0){
				foreach ($obj as $userid => $userinfo)
				{
					if ($userid != $g_userid){
					?>
					<option value="<?php echo $userid; ?>"><?php echo $userid; ?></option>
			<?php	}
				}
			}
			?>				
			</select> </div>
		</div>
		

		<div class="account_list">
			<input type="hidden" name="market_close" value="-1" />
			<table cellpadding="0" cellspacing="0" border="0">
			 <tr>
				<th>&nbsp;</th>
				<th>Accounts</th>
				<th>Position</th>
				<th>Order</th>
			
				
				<th>Action</th>
			</tr>
			 

<?php 	
	if (sizeof($obj) > 0){
	foreach ($obj as $userid => $userinfo)
	{
		
		//	var_dump($userinfo);
			
		// remote configuration			
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "REMOTE_CONFIGURATION";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					
					$remote_config = json_decode($result);
			//		var_dump($remote_config);
				//["forceSettle"]=> string(5) "false" ["breakPt"]=> string(2) "-1" ["reverse"]=> string(5) "false" ["forcePosition"]=> string(0) "" ["forceOpen"]=> string(5) "false" 		
				//	echo $result->forceOpen;
				/*	$remote_config->forceSettle;
					$remote_config->breakPt;
					$remote_config->reverse;
					$remote_config->forcePosition;				
					$remote_config->forceOpen;*/
				}

				
				
		// active order

			//	var_dump($json);
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ACTIVE_ORDER";
				$json["value"] = "TRUE";
				
				$active_order = array();
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$active_order = json_decode($result);
				//	var_dump($result);
					
				}
				
		//balance		
				
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "BALANCE";
				$json["value"] = "TRUE";
				

				$balance = array();
				$connection = new Connection();			
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$balance = json_decode($result);
				//	var_dump($balance);
				}
				
				//var_dump($remote_config, $active_order, $balance);

		//algo status		
				
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ALGO_STATUS";
				$json["value"] = "TRUE";
				$json["sync"] = "FALSE";
				

				$connection = new Connection();			
				$connection->setconnect($conn_r);
				$algo_status =  trim($connection->sendcommand($json));	

		// order statistics

			//	var_dump($json);
				$json = array();
				$json["account"] = $userid;
				$json["action"] = "ORDER_STATISTICS";
				$json["value"] = "TRUE";
				$json["sync"] = "FALSE";
				
				$order_stats = array();
				$buycount = 0;
				$sellcount = 0;
				$product = "-";
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	

				if ($result != "")
				{
					$order_stats = json_decode($result);
					$order_stats = json_decode($order_stats);
					//var_dump($result ,$order_stats);
					if ($order_stats){
					foreach ($order_stats as $stats){
						$buycount += $stats->buyCnt;
						$sellcount += $stats->sellCnt;
						$productproduct = $stats->prodCde;
					}
					}
				}
		// Open order
		$json = array();
		$json["account"] = $g_userid;
		$json["action"] = "OPEN_ORDER";
		$json["value"] = "TRUE";
		

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if ($result != "")
		{
			$open_record = json_decode($result);
		}
		$profitThreshold = "-";
		$lossThreshold = "-";
		$trailingStopRatio = "-";
		if (sizeof($open_record) > 0){
				foreach ($open_record as $rec){ 
					$recordinfo = $rec->orderList[0];
					$profitThreshold = $recordinfo->profitThreshold;
					$lossThreshold = $recordinfo->lossThreshold;
					$trailingStopRatio = $recordinfo->trailingStopRatio;
				}
		}
					
			
		
	//	if ($userid == "shadowTest" ){$userinfo->viewonly = 1; }
?>
		<form method="POST" action="" >
		
		<tr class="row <?php echo $userid == $g_userid ? "parent" : ""; ?>">
			<?php
			

				//echo $algo_status;
				$status_title = "-";
				$status_color = "";
				$status_algo = "";
			
				if ($algo_status == "EMPTY" || $algo_status == "0"){
					$status_title =  "Ready";
					$status_color = "green";
				}
				if ($algo_status == "EXECUTING" ){
					if ($active_order){
						$netprice = $market_data->close - $active_order->commitAvgPrice; // LONG
						if ($active_order->position == "SHORT"){
							$netprice = $netprice * -1;
						}
						
						$netprice = $netprice > 0 ? "+".$netprice : $netprice;
						$status_title =  $active_order->position." ".$active_order->commitAvgPrice." <b>(".$netprice.")</b>";
						$status_color = ($netprice > 0 ? "green" : "red");
						$status_algo =  ($active_order->position == "LONG" ? "call" : "put");
					}else{
						$status_title =  "Ready";
						$status_color = "green";
					}
					
				}else if ($algo_status == "END"){
					$status_title =  "Game Over";
					$status_color = "red";
				}else{
					$status_title =  "-";
				}
				
				// warning for failed stats
				if (($algo_status != "EXECUTING" && $algo_status != "END") && $buycount != $sellcount){
					$status_title = "***WARNING***<br>".$status_title;
					$status_color = "red";
				}
				
				// total text
				$total = 0;
				$price_value = 10;
				if ($balance != 0){
					if ($active_order && $active_order->marketDataKey->product == "HSI"){
						$price_value = 50;
					}
					$total = $balance * $price_value;
				}
				$totaltext = "$".number_format($total, 2);
				if ($total < 0){
					$totaltext = "(".$totaltext.")";
				}
			?>
		
			<input type="hidden" name="price_value" value="<?php echo $price_value;?>">
			<input type="hidden" name="userid" value="<?php echo $userid; ?>" />
 			<input type="hidden" name="server" value="<?php echo $userinfo->server; ?>"/>
			<input type="hidden" name="port" value="<?php echo $userinfo->port; ?>"/>
			<input type="hidden" name="sync" value="0">
			<input type="hidden" name="algo_status" value="<?php echo $algo_status; ?>">
			<input type="hidden" name="active_order" value="0">
			<input type="hidden" name="order_stats" value="0">
			<input type="hidden" name="viewonly" value="<?php echo $userinfo->viewonly == 1 ? "1" : "0"; ?>">
			
			<td class="chk">
			<?php if ($userinfo->viewonly != 1) { ?>
			<input type="checkbox" name="acc_check" checked <?php echo $userinfo->viewonly == 1 ? "disabled" : ""; ?> />
			<?php } ?>
			</td>

			<td class="user">
			
				<?php echo $userid;?>
			</td>
			
			<td>
			<?php 
			if (sizeof($order_stats) > 0){ ?>
			<span class="stats"> <?php echo $product . " | Buy : ". $buycount. " | Sell : ". $sellcount; ?></span>
			<?php } ?>
			</td>
			
			<td class="trade_status <?php echo $status_color; ?>">
				<?php echo $status_title; ?>
			</td>
			
			<td class="point <?php echo $balance > 0 ? "green" : ($balance < 0 ? "red" : ""); ?>">
			<?php echo $balance > 0 ? "+".$balance : $balance; ?>
			
			</td>
			<td class="balance  <?php echo $balance > 0 ? "green" : ($balance < 0 ? "red" : ""); ?>">
			<?php 	
				echo $totaltext;
			?>
			</td>
		
			
		<?php /*	<td><?php echo $profitThreshold;?></td>
			<td><?php echo $lossThreshold;?></td>
			<td><?php echo $trailingStopRatio;?></td> */ ?>
			
			<td class="action_bar <?php echo $userinfo->viewonly == "1" ? "viewonly" : ""; ?>">

				<div class="switch-wrapper">
					<?php 
						$algo_on = 0;
						if ($remote_config){
							if ($remote_config->forcePosition == "LONG" || $remote_config->forcePosition == "SHORT"){
								$algo_on = 1;
							}else if ($remote_config->forcePosition == "NIL"){
								$algo_on = 0;
							}else{
								$algo_on = 1;
							}
						}
					
					?>
				
					<input type="checkbox" name="btnalgo" value="1" <?php echo $algo_on ? "checked" : ""; ?> >
				</div>	
		  
				<input type="submit" class="btncall <?php echo $status_algo == "call" ? "on" : ""; ?>" name="Call" value="Call" ></input>
			
				
				<input type="submit" class="btnsettle <?php echo $status_algo != "" ? "on" : ""; ?>" name="Settle" value="Settle" ></input>
				
					<input type="submit" class="btnput <?php echo $status_algo == "put" ? "on" : ""; ?>" name="Put" value="Put" ></input>
			
			<?php if ($userinfo->viewonly == "1" ){ ?>
				<div class="grey">
					View ONLY
				</div>
			<?php } ?>
			</td>
			
		</tr>
		</form>
<?php 
	}  
	}else{
	?>
		<div class="norecord">Connection failed</div>
	
	<?php } ?>
		</div>
	
		
	</div>	
	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	
    <script src="../js/jquery.switchButton.js"></script>
	<script src="../js/jquery.number.js"></script>

    <script>
      $(function() {
		  
		var url = "OmniController.php"; 
		var market_close = 0;
		var refresh_rate = <?php echo refresh_rate(5); ?>;
		var refresh_rate_30 = <?php echo refresh_rate(30); ?>;
		var refresh_rate_min = <?php echo refresh_rate(60); ?>;
		  
		setInterval(function () {refreshAccountList()}, refresh_rate);//request every x seconds
		
		setInterval(function () {refreshOrderStatsProc()}, refresh_rate_min);//request every x seconds

		setInterval(function () {getOrderList()}, refresh_rate_30);
	
		
        $(".switch-wrapper input").switchButton({
          width: 50,
          height: 20,
		  button_width: 20
        });
		
		$(".switch-wrapper input[name=btnalgo]").each(function(){  
			if ($(this).is(':checked')){
				$(this).parent().find(".switch-button-background").css("background-color", "#3ae428");
			}else{
				$(this).parent().find(".switch-button-background").css("background-color", "#ccc");
			}
		});
		
			
		$(".switch-wrapper input[name=btnalgo]").change(function(){
			
			
			var userid = $(this).parent().closest(".row").find("input[name='userid']").val();
			var port = $(this).parent().closest(".row").find("input[name='port']").val();
			var server = $(this).parent().closest(".row").find("input[name='server']").val();
			
			console.log($(this).is(":checked"));
			if ($(this).is(':checked')){
				$(this).parent().find(".switch-button-background").css("background-color", "#3ae428");
				
				// reset algo
				var data = {};
				data.action = "reset";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "NIL";
				data.sync = false;
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							//console.log(json);
							
						}
					}
				);
				
			}else{
				$(this).parent().find(".switch-button-background").css("background-color", "#ccc");
				
				
				// off algo
				var data = {};
				data.action = "force_position";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "NIL";
				data.sync = false;
				
				//console.log(data);
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							//console.log(json);
							
						}
					}
				);
			}
		});
		
		$(".action_bar .btncall").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
		
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			
			console.log(userid +" call");
			
			// active order?
				var data = {};
				data.action = "force_open";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "LONG";
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;

				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							refreshAccountList();
							console.log(json);
							
						}
					}
				);
		});
		
		$(".action_bar .btnput").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			console.log(userid +" put");
			
			// active order?
				var data = {};
				data.action = "force_open";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "SHORT";
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;
				
				console.log(data);
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							refreshAccountList();
							console.log(json);
							
						}
					}
				);
		});
		
		$(".action_bar .btnsettle").click(function(){
			
			var sync = $(this).parent().parent().find("input[name='sync']").val();
			
			var userid = $(this).parent().parent().find("input[name='userid']").val();
			var port = $(this).parent().parent().find("input[name='port']").val();
			var server = $(this).parent().parent().find("input[name='server']").val();
			console.log(userid +" settle");
			
			// active order?
				var data = {};
				data.action = "force_settle";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				data.sync = false; //(typeof sync === 'undefined' || sync == '0') ? false : sync;

				
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							$(this).parent().parent().find("input[name='sync']").val(false);
							refreshAccountList();
							console.log(json);
							
						}
					}
				);
		});
		
		function refreshRemoteConfig(row, userid, port, server){
				
				var data = {};
				data.action = "remote_configuration";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				
			
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
						}
					}
				);
		}
		
		function refreshAlgoStatus(row, userid, port, server){
			// algo status
				var data = {};
				data.action = "algo_status";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				data.sync = false;
				
				
				$.post(
					url,
					data,
					function(json) {
						console.log(json);
							
						if (json.ok == 1){
						
							
							console.log(userid + "=>"+$(row).find("input[name='algo_status']").val());
							console.log(userid + "<=" + json.status);
							$(row).find("input[name='algo_status']").val(json.status);
							
						}
				
					}
				);
		}
		
		function refreshActiveOrder(row, userid, port, server){
			// active order?
				var data = {};
				data.action = "active_order";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = true;
				
				
				
			//	console.log(userid + " === "+algo_status);
				algo_status = $(row).find("input[name='algo_status']").val().trim();
				market_close = $(".account_list input[name=market_close]").val();
				if (algo_status == "EMPTY" || algo_status == "0"){
						status_title =  "Ready";
						status_color = "green";
				}else if (algo_status == "END"){
							status_title =  "Game Over";
							status_color = "red";
				}
				
				$(row).find(".trade_status").html(status_title);				
				if (status_color == "red"){
					$(row).find(".trade_status").removeClass("green");
					$(row).find(".trade_status").addClass("red");
				}else{
					$(row).find(".trade_status").removeClass("red");
					$(row).find(".trade_status").addClass("green");
				}
			
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							
							algo_status = $(row).find("input[name='algo_status']").val().trim();
							market_close = $(".account_list input[name=market_close]").val();
							
							if (json.product == "HSI"){
								$(row).find("input[name='price_value']").val(50);
							}else{
								$(row).find("input[name='price_value']").val(10);					
							}
								
						}
						
						console.log(userid + " === "+algo_status);
				
						console.log(algo_status + json.position + "--" + json.commitAvgPrice + "--" + json.product);
						
						//echo $algo_status;
						var status_title = "-";
						var status_color = "";
						
						if (algo_status == "EMPTY" || algo_status == "0"){
								status_title =  "Ready";
								status_color = "green";
						}
						
						if (algo_status == "EXECUTING"  ){
							if (json.position != "NIL"){
								var netprice = market_close - json.commitAvgPrice; // LONG
								if (json.position == "SHORT"){
									netprice  = netprice*-1;
								}

								netprice = netprice > 0 ? "+" + netprice : netprice;
								status_title =  json.position + " " + json.commitAvgPrice+ " <b>(" +netprice+")</b>";
								console.log(status_title);
								status_color = (netprice > 0 ? "green" : "red");
								//status_algo =  ($active_order->position == "LONG" ? "call" : "put");
							}else{
								status_title =  "Ready";
								status_color = "green";
							}
							
						}else if (algo_status == "END"){
							status_title =  "Game Over";
							status_color = "red";
						}
						
						
						$(row).find(".trade_status").html(status_title);
						if (status_color == "red"){
							$(row).find(".trade_status").removeClass("green");
							$(row).find(".trade_status").addClass("red");
						}else{
							$(row).find(".trade_status").removeClass("red");
							$(row).find(".trade_status").addClass("green");
						}
							
						console.log(userid + " : "+status_title);
						
					}
				);		
						
							
					
				 
			
							

					
		}
		
		function refreshBalance(row, userid, port, server){
				// balance
				var data = {};
				data.action = "balance";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				price_value = $(row).find("input[name='price_value']").val();
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							var balance = json.balance;
							if (balance > 0){
								balance = "+"+balance;
								$(row).find(".point").removeClass("red");
								$(row).find(".point").addClass("green");
								
								$(row).find(".balance").removeClass("red");
								$(row).find(".balance").addClass("green");
																
							}else{
								$(row).find(".point").removeClass("green");
								$(row).find(".point").addClass("red");
																
								$(row).find(".balance").removeClass("green");
								$(row).find(".balance").addClass("red");
							}
							$(row).find(".point").text(balance); 
							
							var total = balance * price_value;
							var totaltext = "$" + $.number(total,2);
							if (total < 0){
								totaltext = "(" + totaltext + ")";
							}
							$(row).find(".balance").text(totaltext); 
						//	console.log(json);
						}
					}
				);
		}
		
		
		function refreshOrderStatsProc(){
			
			market_close = $(".account_list input[name=market_close]").val();
				
			$(".row").each(function(){
				
				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				
				var row = $(this).parent();
			
				
				refreshOrderStats(row, userid, port, server);
				
				
				
			});
			
		}
		
		function refreshOrderStats(row, userid, port, server){
				// balance
				var data = {};
				data.action = "order_statistics";
				data.host = server;
				data.port = port;
				data.userid = userid;
				data.value = "TRUE";
				
				order_stats = $(row).find("input[name='order_stats']").val();
				
				$.post(
					url,
					data,
					function(json) {
						
						
							$(row).find(".order_stats").text(json.ordertext); 
						//	console.log(json);
						
					}
				);
		}
		
		function refreshAccountList(){
			
			market_close = $(".account_list input[name=market_close]").val();
				
			$(".row").each(function(){
				
				var userid = $(this).find("input[name='userid']").val();
				var port = $(this).find("input[name='port']").val();
				var server = $(this).find("input[name='server']").val();
				
				var row = this;
			
				
				//console.log(userid, port, server);
				
				refreshRemoteConfig(row, userid, port, server);
				refreshAlgoStatus(row, userid, port, server);
				refreshActiveOrder(row, userid, port, server);
				refreshBalance(row, userid, port, server);
				
				
				
			});
			
		}
		
		// Sync
		
		$(".cp_all").change(function(){
			console.log($(this).is(":checked"));
			if ($(this).is(":checked")){
				$(".user input[name=acc_check]").prop("checked", true);
			}else{
				$(".user input[name=acc_check]").prop("checked", false);
			}
		});
		
		$("#cp_call").click(function(){
			
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
			
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btncall").click();
				}
			});
		});
		
		$("#cp_put").click(function(){
			
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btnput").click();
				}
			});

		});
		
		$("#cp_settle").click(function(){
			$(".row").each(function(){
				
				var acc_check = $(this).find(".user input[name=acc_check]").is(":checked");
				var viewonly = $(this).find("input[name=viewonly]").val();
				if (acc_check && viewonly != 1){
					$(this).find("input[name=sync]").val(true);
					$(this).find(".action_bar .btnsettle").click();
				}
			});

		});
		
		
		function getOrderList(){
			var userid = "rickyTest";
			var server = "veeko123.no-ip.org";
			var port = "5987";
			var order_list = [];
			
			// get open order
			var data = {};
			data.action = "open_order";
			data.host = server;
			data.port = port;
			data.userid = userid;
			data.value = "TRUE";
				
			$.post(
					url,
					data,
					function(json) {
						
						order_list.push(json.open_order);
						
						//	$(row).find(".order_stats").text(json.ordertext); 
						//	console.log(json);
						
				}
			);
			
			// get settled order
			var data = {};
			data.action = "settled_order";
			data.host = server;
			data.port = port;
			data.userid = userid;
			data.value = "TRUE";
				
			$.post(
					url,
					data,
					function(json) {
						
						order_list.push(json.settled_order);
						
						//	$(row).find(".order_stats").text(json.ordertext); 
						//	console.log(json);
						
				}
			);
			
			// get pending order
			var data = {};
			data.action = "pending_order";
			data.host = server;
			data.port = port;
			data.userid = userid;
			data.value = "TRUE";
				
			$.post(
					url,
					data,
					function(json) {
						
						order_list.push(json.settled_order);
						
						//	$(row).find(".order_stats").text(json.ordertext); 
						//	console.log(json);
						
				}
			);
			
			console.log(order_list);
			
		}
		
      });
   
	
	
	</script>
	
  </body>
</html>