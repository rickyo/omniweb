<?php

class OmniOptionSet
{

	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		
	}
	
	public function getStrageties($criteria=array())
	{
	
			$sql = "SELECT * FROM omni_option_set ";
		
			$data_arr = array();
			if (isset($criteria["status"])){
				array_push($data_arr, "`status` = '".intval($criteria["status"])."'");
			}
			
			if (sizeof($data_arr) > 0){
				$sql .= " WHERE ".implode(" AND ", $data_arr);
			}
		
			$sql .= " ORDER BY `set_id` ASC ";
			
					
			//echo $sql."<br/>";
			$result = getQuery($sql);
			
			return $result;
	}

	public function create($data = array())
	{


	 $opt_data = array();
		
	  $opt_data["set_id"] = "NULL";
	  $opt_data["set_name"] = isset($data["set_name"])? "'".$this->db->real_escape_string(trim($data["set_name"]))."'" : "''";
	  $opt_data["option_position"] = isset($data["option_position"])? "'".$this->db->real_escape_string(trim($data["option_position"]))."'" : "''";
	  $opt_data["future_position"] = isset($data["future_position"])? "'".$this->db->real_escape_string(trim($data["future_position"]))."'" : "''";
	 
	 $opt_data["decisionidx"] = isset($data["decisionidx"])? "'".$this->db->real_escape_string(trim($data["decisionidx"]))."'" : "''";
	 
	  $opt_data["triggerprice"] = isset($data["triggerprice"])? "'".$this->db->real_escape_string(trim($data["triggerprice"]))."'" : "''";
	 

	  $opt_data["set_trigger"] =  isset($data["set_trigger"])? "'".$this->db->real_escape_string(trim($data["set_trigger"]))."'" : "''";
	  $opt_data["option_strike"] = isset($data["option_strike"])? "'".$this->db->real_escape_string(trim($data["option_strike"]))."'" : "''";
      $opt_data["option_limit_price"] = isset($data["option_limit_price"])? "'".$this->db->real_escape_string(trim($data["option_limit_price"]))."'" : "''";
	  $opt_data["option_qty"] =  isset($data["option_qty"])? intval($data["option_qty"]) : 0;
      $opt_data["future_market_price"] = isset($data["future_market_price"])? "'".$this->db->real_escape_string(trim($data["future_market_price"]))."'" : "''";	  
	  $opt_data["future_qty"] =  isset($data["future_qty"])? intval($data["future_qty"]) : 0;
	  $opt_data["cut_gain"] =  isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
	  $opt_data["cut_loss"] =  isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
	  $opt_data["trailstop"] =  isset($data["trailstop"])? intval($data["trailstop"]) : 0;
	   $opt_data["set_num"] =  isset($data["set_num"])? intval($data["set_num"]) : 0;
	  $opt_data["created_date"] = isset($data["created_date"])? (strtolower($data["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
	  $opt_data["modified_date"] = isset($data["modified_date"])? (strtolower($data["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
	 $opt_data["status"] =  isset($data["status"])? intval($data["status"]) : 0;
	

		$sql = "INSERT INTO "."omni_option_set"." (";
		$sql .= implode(",", array_keys($opt_data));
		$sql .= ") VALUES (";
		$sql .= implode("," ,$opt_data);
		$sql .= ")";
	   
	//   echo $sql;
	   
		$result = getQuery($sql);
		$set_id = getInsertID();
		
		return $set_id;
		
	}
	
	
	
	public function update($criteria = array())
	{
		$fields = array();
		
		if (isset($criteria["set_name"])  && trim($criteria["set_name"]) != ""){
			$fields["set_name"] = ( trim($criteria["set_name"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["set_name"]))."'");
		}
		
		if (isset($criteria["option_position"])  && trim($criteria["option_position"]) != ""){
			$fields["option_position"] = ( trim($criteria["option_position"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["option_position"]))."'");
		}
		
		if (isset($criteria["future_position"])  && trim($criteria["future_position"]) != ""){
			$fields["future_position"] = ( trim($criteria["future_position"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["future_position"]))."'");
		}
		
		if (isset($criteria["decisionidx"])  && trim($criteria["decisionidx"]) != ""){
			$fields["decisionidx"] = ( trim($criteria["decisionidx"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["decisionidx"]))."'");
		}
		
		if (isset($criteria["triggerprice"])  && trim($criteria["triggerprice"]) != ""){
			$fields["triggerprice"] = ( trim($criteria["triggerprice"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["triggerprice"]))."'");
		}
		
		if (isset($criteria["set_trigger"])  && trim($criteria["set_trigger"]) != ""){
			$fields["set_trigger"] = ( trim($criteria["set_trigger"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["set_trigger"]))."'");
		}
		
		if (isset($criteria["option_strike"])  && trim($criteria["option_strike"]) != ""){
			$fields["option_strike"] = ( trim($criteria["option_strike"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["option_strike"]))."'");
		}

		if (isset($criteria["option_limit_price"])  && trim($criteria["option_limit_price"]) != ""){
			$fields["option_limit_price"] = ( trim($criteria["option_limit_price"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["option_limit_price"]))."'");
		}		
		if (isset($criteria["option_qty"])  && trim($criteria["option_qty"]) != ""){
		$fields["option_qty"] = isset($criteria["option_qty"])? intval($criteria["option_qty"]) : 0;
		}		
		
		if (isset($criteria["future_market_price"])  && trim($criteria["future_market_price"]) != ""){
			$fields["future_market_price"] = ( trim($criteria["future_market_price"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["future_market_price"]))."'");
		}	
		
		if (isset($criteria["future_qty"])  && trim($criteria["future_qty"]) != ""){
			$fields["future_qty"] = isset($criteria["future_qty"])? intval($criteria["future_qty"]) : 0;
		}
		
		if (isset($criteria["created_date"])  && trim($criteria["created_date"]) != ""){
			$fields["created_date"] = isset($criteria["created_date"])?(strtolower($criteria["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($criteria["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($criteria["cut_gain"])  && trim($criteria["cut_gain"]) != ""){
			$fields["cut_gain"] = isset($criteria["cut_gain"])? intval($criteria["cut_gain"]) : 0;
		}
		
		if (isset($criteria["cut_loss"])  && trim($criteria["cut_loss"]) != ""){
			$fields["cut_loss"] = isset($criteria["cut_loss"])? intval($criteria["cut_loss"]) : 0;
		}
		
		
		if (isset($criteria["trailstop"])  && trim($criteria["trailstop"]) != ""){
			$fields["trailstop"] = isset($criteria["trailstop"])? intval($criteria["trailstop"]) : 0;
		}
		
		if (isset($criteria["set_num"])  && trim($criteria["set_num"]) != ""){
			$fields["set_num"] = isset($criteria["set_num"])? intval($criteria["set_num"]) : 0;
		}
		
		if (isset($criteria["modified_date"])  && trim($criteria["modified_date"]) != ""){
			$fields["modified_date"] = isset($criteria["modified_date"])?(strtolower($criteria["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($criteria["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($criteria["status"])){
			$fields["status"] = isset($criteria["status"])? intval($criteria["status"]) : 0;
		}
		
	/*	if (isset($criteria["campaign_name"])  && trim($criteria["campaign_name"]) != ""){
			$fields["campaign_name"] = ( trim($criteria["campaign_name"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["campaign_name"]))."'");
		}
		
		if (isset($criteria["source"])  && trim($criteria["source"]) != ""){
			$fields["source"] = ( trim($criteria["source"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["source"]))."'");
		}
		
		if (isset($criteria["url"])  && trim($criteria["url"]) != ""){
			$fields["url"] = ( trim($criteria["url"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["url"]))."'");
		}
		
		if (isset($criteria["tracking_url"])  && trim($criteria["tracking_url"]) != ""){
			$fields["tracking_url"] = ( trim($criteria["tracking_url"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["tracking_url"]))."'");
		}
		
		if (isset($criteria["count"]) && intval($criteria["count"]) > 0){
			$fields["count"] =  "count + 1";
		}
		
		if (isset($criteria["status"])  && trim($criteria["status"]) != ""){
			$fields["status"] = intval($criteria["status"]);
		}
		*/
		
		if (sizeof($fields) > 0)
        {		
				
            $sql = "UPDATE omni_option_set SET ";
            $tmp = array();
            foreach ($fields as $title => $val){
				$field_data_arr[$title] = $val;
                array_push($tmp , $title." = ".$val);
            }
            $sql .= implode(",", $tmp);
           
            $condition = array();
                               
            if (isset($criteria["set_id"]))
                array_push($condition,  "set_id = ".$criteria["set_id"]);
	              
			if (sizeof($condition) > 0)
				$sql .= " WHERE ".implode(" AND ", $condition);

			
			
            $result = getQuery($sql);
			

          //  echo $sql;
           
            //$result = $this->getConnection()->query($sql);
        }
	}
	
	public function addOption($data){
		
		$opt_data = array();
		
		//	opt_id 	strategy_set_id 	option_position 	option_strike 	option_limit_price 	option_qty 	cut_gain 	cut_loss 	trailstop 	set_num 	created_date 	modified_date 	status
			
		  $opt_data["opt_id"] = "NULL";
		  $opt_data["strategy_set_id"] =  isset($data["strategy_set_id"])? intval($data["strategy_set_id"]) : 0;
		
		  $opt_data["option_position"] = isset($data["option_position"])? "'".$this->db->real_escape_string(trim($data["option_position"]))."'" : "''";
		  $opt_data["option_strike"] = isset($data["option_strike"])? "'".$this->db->real_escape_string(trim($data["option_strike"]))."'" : "''";
		  $opt_data["option_limit_price"] = isset($data["option_limit_price"])? "'".$this->db->real_escape_string(trim($data["option_limit_price"]))."'" : "''";
		  $opt_data["instrument"] = isset($data["instrument"])? "'".$this->db->real_escape_string(trim($data["instrument"]))."'" : "''";
		  $opt_data["product"] = isset($data["product"])? "'".$this->db->real_escape_string(trim($data["product"]))."'" : "''";
		  $opt_data["contractMonth"] = isset($data["contractMonth"])? "'".$this->db->real_escape_string(trim($data["contractMonth"]))."'" : "''";
		  
		  $opt_data["option_qty"] =  isset($data["option_qty"])? intval($data["option_qty"]) : 0;
		  $opt_data["cut_gain"] =  isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
		  $opt_data["cut_loss"] =  isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
		  $opt_data["trailstop"] =  isset($data["trailstop"])? intval($data["trailstop"]) : 0;
		  
		  $opt_data["set_num"] =  isset($data["set_num"])? intval($data["set_num"]) : 0;
		  $opt_data["created_date"] = isset($data["created_date"])? (strtolower($data["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
		  $opt_data["modified_date"] = isset($data["modified_date"])? (strtolower($data["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
		 $opt_data["status"] =  isset($data["status"])? intval($data["status"]) : 0;
		 
		 

		$sql = "INSERT INTO "."omni_option"." (";
		$sql .= implode(",", array_keys($opt_data));
		$sql .= ") VALUES (";
		$sql .= implode("," ,$opt_data);
		$sql .= ")";
	   
	   
		$result = getQuery($sql);
		$opt_id = getInsertID();
		
		return $opt_id;
	}
	
	public function addFuture($data){
		
		$future_data = array();
		
		
//future_id	strategy_set_id	future_position	future_market_price	future_qty	cut_gain	cut_loss	trailstop	set_num	created_date	modified_date	status

		  $future_data["future_id"] = "NULL";
		  $future_data["strategy_set_id"] =  isset($data["strategy_set_id"])? intval($data["strategy_set_id"]) : 0;
		
		  $future_data["future_position"] = isset($data["future_position"])? "'".$this->db->real_escape_string(trim($data["future_position"]))."'" : "''";
	 
	

	  
      $future_data["future_market_price"] = isset($data["future_market_price"])? "'".$this->db->real_escape_string(trim($data["future_market_price"]))."'" : "''";	  
	   $future_data["contractMonth"] = isset($data["contractMonth"])? "'".$this->db->real_escape_string(trim($data["contractMonth"]))."'" : "''";
	     $future_data["product"] = isset($data["product"])? "'".$this->db->real_escape_string(trim($data["product"]))."'" : "''";
	  $future_data["future_qty"] =  isset($data["future_qty"])? intval($data["future_qty"]) : 0;
	  $future_data["cut_gain"] =  isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
	  $future_data["cut_loss"] =  isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
	  $future_data["trailstop"] =  isset($data["trailstop"])? intval($data["trailstop"]) : 0;
	   $future_data["set_num"] =  isset($data["set_num"])? intval($data["set_num"]) : 0;
	   
	  $future_data["created_date"] = isset($data["created_date"])? (strtolower($data["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
	  $future_data["modified_date"] = isset($data["modified_date"])? (strtolower($data["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
	 $future_data["status"] =  isset($data["status"])? intval($data["status"]) : 0;
		 

		$sql = "INSERT INTO "."omni_future"." (";
		$sql .= implode(",", array_keys($future_data));
		$sql .= ") VALUES (";
		$sql .= implode("," ,$future_data);
		$sql .= ")";
	   
	   
		$result = getQuery($sql);
		$future_id = getInsertID();
		
		return $future_id;
	}

	public function getOptionList($criteria=array())
	{
	
			$sql = "SELECT * FROM omni_option ";
		
			$data_arr = array();
			if (isset($criteria["strategy_set_id"])){
				array_push($data_arr, "`strategy_set_id` = '".intval($criteria["strategy_set_id"])."'");
			}
			
			if (isset($criteria["status"])){
				array_push($data_arr, "`status` = '".intval($criteria["status"])."'");
			}
			
			if (sizeof($data_arr) > 0){
				$sql .= " WHERE ".implode(" AND ", $data_arr);
			}
		
			$sql .= " ORDER BY `opt_id` ASC ";
			
					
			//echo $sql."<br/>";
			$result = getQuery($sql);
			$option_list = array();
			foreach ($result as $res){
				$option_list[$res["opt_id"]] = $res;
			}
			
			return $option_list;
	}
	
	
	public function getFutureList($criteria=array())
	{
	
			$sql = "SELECT * FROM omni_future ";
		
			$data_arr = array();
			if (isset($criteria["strategy_set_id"])){
				array_push($data_arr, "`strategy_set_id` = '".intval($criteria["strategy_set_id"])."'");
			}
			
			if (isset($criteria["status"])){
				array_push($data_arr, "`status` = '".intval($criteria["status"])."'");
			}
			
			if (sizeof($data_arr) > 0){
				$sql .= " WHERE ".implode(" AND ", $data_arr);
			}
		
			$sql .= " ORDER BY `future_id` ASC ";
			
					
			//echo $sql."<br/>";
			$result = getQuery($sql);
			$future_list = array();
			foreach ($result as $res){
				$future_list[$res["future_id"]] = $res;
			}
			
			return $future_list;
	}
	
	public function updateOptionList($data=array(), $criteria=array())
	{
		//future_id	strategy_set_id	future_position	future_market_price	future_qty	cut_gain	cut_loss	trailstop	set_num	created_date	modified_date	status
		$fields = array();
		
		if (isset($data["strategy_set_id"])  && trim($data["strategy_set_id"]) != ""){
			$fields["strategy_set_id"] = isset($data["strategy_set_id"])? intval($data["strategy_set_id"]) : 0;
		}		
		
		
		
		if (isset($data["option_position"])  && trim($data["option_position"]) != ""){
			$fields["option_position"] = ( trim($data["option_position"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["option_position"]))."'");
		}
		
		
		if (isset($data["instrument"])  && trim($data["instrument"]) != ""){
			$fields["instrument"] = ( trim($data["instrument"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["instrument"]))."'");
		}
	
		if (isset($data["product"])  && trim($data["product"]) != ""){
			$fields["product"] = ( trim($data["product"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["product"]))."'");
		}
		
		if (isset($data["option_strike"])  && trim($data["option_strike"]) != ""){
			$fields["option_strike"] = ( trim($data["option_strike"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["option_strike"]))."'");
		}

		if (isset($data["option_limit_price"])  && trim($data["option_limit_price"]) != ""){
			$fields["option_limit_price"] = ( trim($data["option_limit_price"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["option_limit_price"]))."'");
		}	

		if (isset($data["contractMonth"])  && trim($data["contractMonth"]) != ""){
			$fields["contractMonth"] = ( trim($data["contractMonth"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["contractMonth"]))."'");
		}	
		
		if (isset($data["option_qty"])  && trim($data["option_qty"]) != ""){
		$fields["option_qty"] = isset($data["option_qty"])? intval($data["option_qty"]) : 0;
		}		
		
	
		
		if (isset($data["created_date"])  && trim($data["created_date"]) != ""){
			$fields["created_date"] = isset($data["created_date"])?(strtolower($data["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($data["cut_gain"])  && trim($data["cut_gain"]) != ""){
			$fields["cut_gain"] = isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
		}
		
		if (isset($data["cut_loss"])  && trim($data["cut_loss"]) != ""){
			$fields["cut_loss"] = isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
		}
		
		
		if (isset($data["trailstop"])  && trim($data["trailstop"]) != ""){
			$fields["trailstop"] = isset($data["trailstop"])? intval($data["trailstop"]) : 0;
		}
		
		if (isset($data["set_num"])  && trim($data["set_num"]) != ""){
			$fields["set_num"] = isset($data["set_num"])? intval($data["set_num"]) : 0;
		}
		
		if (isset($data["modified_date"])  && trim($data["modified_date"]) != ""){
			$fields["modified_date"] = isset($data["modified_date"])?(strtolower($data["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($data["status"])){
			$fields["status"] = isset($data["status"])? intval($data["status"]) : 0;
		}

		
		if (sizeof($fields) > 0)
        {		
				
            $sql = "UPDATE omni_option SET ";
            $tmp = array();
            foreach ($fields as $title => $val){
				$field_data_arr[$title] = $val;
                array_push($tmp , $title." = ".$val);
            }
            $sql .= implode(",", $tmp);
           
            $condition = array();
                               
            if (isset($criteria["opt_id"]))
                array_push($condition,  "opt_id = ".$criteria["opt_id"]);
	           
			if (isset($criteria["strategy_set_id"]))
                array_push($condition,  "strategy_set_id = ".$criteria["strategy_set_id"]);
	              			   
				  
			if (sizeof($condition) > 0)
				$sql .= " WHERE ".implode(" AND ", $condition);

			
			
            $result = getQuery($sql);
			

            //echo $sql;
           
            //$result = $this->getConnection()->query($sql);
        }
	}
	
	public function updateFutureList($data=array(), $criteria=array())
	{
		//future_id	strategy_set_id	future_position	future_market_price	future_qty	cut_gain	cut_loss	trailstop	set_num	created_date	modified_date	status
		$fields = array();
		
		if (isset($data["strategy_set_id"])  && trim($data["strategy_set_id"]) != ""){
			$fields["strategy_set_id"] = isset($data["strategy_set_id"])? intval($data["strategy_set_id"]) : 0;
		}		
		
		
	
		
		if (isset($data["future_position"])  && trim($data["future_position"]) != ""){
			$fields["future_position"] = ( trim($data["future_position"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["future_position"]))."'");
		}
		
		
	
		if (isset($data["future_market_price"])  && trim($data["future_market_price"]) != ""){
			$fields["future_market_price"] = ( trim($data["future_market_price"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["future_market_price"]))."'");
		}	
		
		if (isset($data["contractMonth"])  && trim($data["contractMonth"]) != ""){
			$fields["contractMonth"] = ( trim($data["contractMonth"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["contractMonth"]))."'");
		}	
		
		
		if (isset($data["product"])  && trim($data["product"]) != ""){
			$fields["product"] = ( trim($data["product"]) == "" ? "NULL" : "'".$this->db->real_escape_string(trim($data["product"]))."'");
		}	
		
		if (isset($data["future_qty"])  && trim($data["future_qty"]) != ""){
			$fields["future_qty"] = isset($data["future_qty"])? intval($data["future_qty"]) : 0;
		}
		
		
		
		if (isset($data["cut_gain"])  && trim($data["cut_gain"]) != ""){
			$fields["cut_gain"] = isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
		}
		
		if (isset($data["cut_loss"])  && trim($data["cut_loss"]) != ""){
			$fields["cut_loss"] = isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
		}
		
		
		if (isset($data["trailstop"])  && trim($data["trailstop"]) != ""){
			$fields["trailstop"] = isset($data["trailstop"])? intval($data["trailstop"]) : 0;
		}
		
		if (isset($data["set_num"])  && trim($data["set_num"]) != ""){
			$fields["set_num"] = isset($data["set_num"])? intval($data["set_num"]) : 0;
		}
		
		if (isset($data["created_date"])  && trim($data["created_date"]) != ""){
			$fields["created_date"] = isset($data["created_date"])?(strtolower($data["created_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["created_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($data["modified_date"])  && trim($data["modified_date"]) != ""){
			$fields["modified_date"] = isset($data["modified_date"])?(strtolower($data["modified_date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["modified_date"]))."'" ) : "'0000-00-00 00:00:00'";
		}
		
		if (isset($data["status"])){
			$fields["status"] = isset($data["status"])? intval($data["status"]) : 0;
		}

		
		if (sizeof($fields) > 0)
        {		
				
            $sql = "UPDATE omni_future SET ";
            $tmp = array();
            foreach ($fields as $title => $val){
				$field_data_arr[$title] = $val;
                array_push($tmp , $title." = ".$val);
            }
            $sql .= implode(",", $tmp);
           
            $condition = array();
                               
            if (isset($criteria["future_id"]))
                array_push($condition,  "future_id = ".$criteria["future_id"]);
			
			 if (isset($criteria["strategy_set_id"]))
                array_push($condition,  "strategy_set_id = ".$criteria["strategy_set_id"]);
	              
			if (sizeof($condition) > 0)
				$sql .= " WHERE ".implode(" AND ", $condition);

			
			
            $result = getQuery($sql);
			

           // echo $sql;
           
            //$result = $this->getConnection()->query($sql);
        }
	}
	
}

?>

