Readme:

Place Order:
Press “Q or W” to highlight different panel
Use Number Pad to input the data
Press “ * ” go to next box
Press “/” go back to previous box

Use Arrow Up and Down to select/change:
1. Option Call/Put
2. Strike Price (Or enter manually)
3. Limit/Market Price (Linked panel should change automatically when you change one) 

Press “Ctrl+R” to reverse the position
(Option and Future reverse Long Short, color change)

Settle Order (Market):
Press “Q or W” to highlight different panel
Press “Crtl+Space” to settle the trade in
Market price

If the trading panel is “Linked”, first button of "Place order" will be block, 
only can place the Order when you move to the end.