<?php

	date_default_timezone_set('Asia/Hong_Kong');

	$g_userid = "rickyAdmin";
	$g_gmt = 7;

	define("_DEBUG", false );
	$debug_host = 'veeko123.ddns.net';

	$host = 'veeko123.no-ip.org';
	if (_DEBUG){
		$host = $debug_host; 
	}
	
	
	function refresh_rate($sec){
		return $sec*1000;
	}
	
	$ip = gethostbyname($host);
	$port = 5987; 
	$waitTimeoutInSeconds = 1; 
	$buffer_size = 4096000;
	
	$site_dir =  $_SERVER["DOCUMENT_ROOT"];//realpath(dirname(__FILE__)."/");
	$locale = "majestic";
	define('_LOCALE',  $locale);

	define("SITE_PATH", $site_dir."/"._LOCALE);
	define('DIR_SYSTEM', SITE_PATH.'/system');
	define('DIR_CLASS', SITE_PATH.'/class');

	require_once(DIR_SYSTEM . '/database/database.php');
	require_once(DIR_SYSTEM . '/database/connection.php');
	
	// Startup
	require_once(DIR_SYSTEM . '/startup.php');
	// Registry
	$registry = new Registry();
	
	//Notification Database
	/*$nodbname = "wise";
	$nodbhost = "veeko123.no-ip.org";
	$nodbusername = "majestic";
	$nodbpassword = "P@ssw0rd";*/
	
	$nodbname = "majestic";
	$nodbhost = "localhost";
	$nodbusername = "root";
	$nodbpassword = "";
	
		
	$db = myDB::db_database_connect($nodbname,$nodbhost,$nodbusername,$nodbpassword,MYSQLPCONNECT) or die('Unable to connect to database server!');
	ini_set('session.bug_compat_42', 1);
	ini_set('session.bug_compat_warn', 0);
	$registry->set('db', $db);
		
	?>