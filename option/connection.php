<?php

	
class Connection
{

	public $host; 
	public $ip ;
	public $port ; 
	public $waitTimeoutInSeconds ; 
	public $buffer_size ;

	
	public function __construct()
	{
		$this->host = 'veeko123.no-ip.org'; 
		$this->ip = gethostbyname($this->host);
		$this->port = 5987; 
		$this->waitTimeoutInSeconds = 1; 
		$this->buffer_size = 4096000;
	}
	
	public function setconnect($connection_profile)
	{
		$this->host = $connection_profile["host"]; 
		$this->ip = gethostbyname($connection_profile["host"]);
		$this->port = $connection_profile["port"]; 
		
	}



	public function sendcommand($json, $json_force_object=true)
	{

		$buffer = "";
		if(!$socket= @fsockopen($this->ip,$this->port,$errCode,$errStr,$this->waitTimeoutInSeconds)){
			//Connection failed so we display the error number and message and stop the script from continuing
			echo '<p class="error"> error: ' . $errCode . ' ' . $errStr .'</p>';
			die;
		}else{
			
			 // Set timout to 1 second
		  if (!stream_set_timeout($socket, $this->waitTimeoutInSeconds)) die("Could not set timeout");

			if ($json_force_object){
				$http = json_encode($json, JSON_FORCE_OBJECT)."\n";
			}else{
				$http = json_encode($json)."\n";
			}

			
			//Sends are header data to the web server
			//echo "Sending... ";
			//var_dump($http);
			fwrite($socket, $http);		
			//echo "<br>";
			
			while (!feof($socket)) {
				$buffer.= fgets($socket, $this->buffer_size);
				
				if ($buffer != "" && strpos("\n", $buffer) === FALSE) {
				//	echo $buffer;
					//break;
				}
			}

			fclose($socket);

			
		}
		
		return $buffer;
	}

}

?>