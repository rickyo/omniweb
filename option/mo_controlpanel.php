<?php

ini_set("allow_url_fopen", 1);
include_once("config.php");
include_once("connection.php");

include_once("OmniOptionSet.class.php");

$criteria = array("status"=>1);

$optionset = new OmniOptionSet($registry);
$setlist =  $optionset->getStrageties($criteria);

$decision_id = isset($_SESSION["decision_id"]) && intval($_SESSION["decision_id"]) > 0 ? $_SESSION["decision_id"] : 0;
if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = isset($_GET["userid"]) ? $_GET["userid"] : "rickyTest";

	$host = isset($_GET["host"])?$_GET["host"] : "";
	$ip = gethostbyname($host);
	$port = isset($_GET["port"])?$_GET["port"]:"";
}



if (isset($_POST) && sizeof($_POST) > 0){

	if (isset($_POST["add_option_action"]) && $_POST["add_option_action"] == "+"){
		if (isset($_POST["set_id"]) && $_POST["set_id"]>0){
			$optionset->addOption(array("strategy_set_id" => $_POST["set_id"], "status"=>1));
			
			header("location: mo_controlpanel.php?set_id=".$_POST["set_id"]);
		}
	}
	
	
	if (isset($_POST["add_future_action"]) && $_POST["add_future_action"] == "+"){
		if (isset($_POST["set_id"]) && $_POST["set_id"]>0){
			$optionset->addFuture(array("strategy_set_id" => $_POST["set_id"], "status"=>1));
			
			header("location: mo_controlpanel.php?set_id=".$_POST["set_id"]);
		}
	}
	
	if (isset($_POST["del_option_action"]) && intval($_POST["del_option_action"]) > 0)
	{
		var_dump($_POST["del_option_action"]);
		$opt_data = array();
		$opt_data["status"] = 0;								
		$opt_data["modified_date"] = date("Y-m-d H:i:s");
		$optionset->updateOptionList($opt_data, array("opt_id"=>$_POST["del_option_action"]));
		
		header("location: mo_controlpanel.php?set_id=".$_POST["set_id"]);
	}
	
	if (isset($_POST["del_future_action"]) && intval($_POST["del_future_action"]) > 0)
	{
		var_dump($_POST["del_future_action"]);
		$future_data = array();
		$future_data["status"] = 0;								
		$future_data["modified_date"] = date("Y-m-d H:i:s");
		$optionset->updateFutureList($future_data, array("future_id"=>$_POST["del_future_action"]));
		
		header("location: mo_controlpanel.php?set_id=".$_POST["set_id"]);
	}

	if (isset($_POST["set_action"])){
		
		switch ($_POST["set_action"]){
			
			case "Save": 
				$set_id = isset($_POST["set_id"]) ? $_POST["set_id"] : 0;
				$data = $_POST;
				
				$data["created_date"] = "NOW()";
				$data["status"] = 1;
				
				
				if ($set_id > 0){
					
					$setdata = $data;
					unset($setdata["option"]);
					unset($setdata["future"]);
					$optionset->update($setdata);
					if (isset($_POST["option"]) && sizeof($_POST["option"]) > 0){
					
					
						foreach ($_POST["option"] as $opt_id => $opt_data){
							var_dump($opt_data);
							$opt_data["status"] = 1;								
							$opt_data["modified_date"] = date("Y-m-d H:i:s");
							$optionset->updateOptionList($opt_data, array("strategy_set_id"=>$_POST["set_id"], "opt_id"=>$opt_data["optionId"]));
						}
						
						foreach ($_POST["future"] as $future_id => $future_data){
							var_dump($future_data);
							$future_data["status"] = 1;								
							$future_data["modified_date"] = date("Y-m-d H:i:s");
							$optionset->updateFutureList($future_data, array("strategy_set_id"=>$_POST["set_id"], "future_id"=>$future_data["futureId"]));
						}
					}
					
				}else{		
					$optionset->create($data);

				}

				header("location: mo_controlpanel.php?set_id=".$set_id);
				
				break;
			
			case "Remove":
				if (isset($_POST["set_id"]) && $_POST["set_id"]>0){
			
					$set_id = isset($_POST["set_id"]) ? $_POST["set_id"] : 0;
					$data = $_POST;	
					$data["modified_date"] = "NOW()";
					$data["status"] = 0;
					$optionset->update($data);
				
				
					header("location: mo_controlpanel.php");
				}
				break;
				
			case "Open Order":	
			
			
				$conn_r = array("host" => $host, "ip" => $ip, "port" => $port );

			
	/*		{"account":"veekoExtrema","action":"PLACE_COMBO_ORDER","value":"{\"name\":\"test combo\",\"cutGain\":500,\"cutLoss\":250,\"trailStop\":80,\"noOfSet\":5,\"id\":0,\"orderList\":[{\"account\":\"D\",\"action\":\"ADD\",\"marketDataKey\":{\"source\":\"REAL_TIME\",\"product\":\"MHI\",\"instrument\":\"FUTURE\",\"type\":\"TICK\",\"contractMonth\":\"SPOT\",\"strikePrice\":0,\"periodLength\":0,\"contractCalendarMonth\":0,\"deciamFormat\":\"#\"},\"position\":\"LONG\",\"orderPrice\":32100.0,\"orgOrderPrice\":32100.0,\"orderQty\":1,\"orderType\":\"Limit\",\"commitAvgPrice\":0.0,\"commitQty\":0,\"status\":\"New\",\"workerId\":-1,\"wrapperNo\":0,\"lastSentWrapperNo\":-1,\"ackWrapperNo\":-1,\"seqId\":0,\"debugId\":5404432,\"tPlus1\":false,\"orderTime\":-1,\"profitThreshold\":0.0,\"lossThreshold\":0.0,\"trailingStopRatio\":0.0},{\"account\":\"D\",\"action\":\"ADD\",\"marketDataKey\":{\"source\":\"REAL_TIME\",\"product\":\"HSI\",\"instrument\":\"OPTION_CALL\",\"type\":\"PRICE\",\"contractMonth\":\"SPOT\",\"strikePrice\":32200,\"periodLength\":0,\"contractCalendarMonth\":0,\"deciamFormat\":\"#.##\"},\"position\":\"LONG\",\"orderPrice\":115.0,\"orgOrderPrice\":115.0,\"orderQty\":1,\"orderType\":\"Limit\",\"commitAvgPrice\":0.0,\"commitQty\":0,\"status\":\"New\",\"workerId\":-1,\"wrapperNo\":0,\"lastSentWrapperNo\":-1,\"ackWrapperNo\":-1,\"seqId\":0,\"debugId\":824586,\"tPlus1\":false,\"orderTime\":-1,\"profitThreshold\":0.0,\"lossThreshold\":0.0,\"trailingStopRatio\":0.0}],\"decisionType\":\"OPEN\"}","sync":false}*/

	
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "PLACE_COMBO_ORDER";
				$json["sync"] = false;
				
			/*	$decisionList = array();
				$decisionList["name"] = $_POST["set_name"];
				$decisionList["cutGain"] = $_POST["cut_gain"];
				$decisionList["cutLoss"] = $_POST["cut_loss"];
				$decisionList["trailStop"] = $_POST["trailstop"];
				$decisionList["noOfSet"] = $_POST["set_num"];
				$decisionList["datetime"] = date("Y-m-d H:i:s");
				
				$optionList = array();
				$optionList["position"] = $_POST["option_position"];
				$optionList["strike_price"] = $_POST["option_strike"];
				$optionList["qty"] = $_POST["option_qty"];
				$optionList["limit_price"] = $_POST["option_limit_price"];
				
				$futureList = array();
				$futureList["position"] = $_POST["future_position"];
				$futureList["price"] = $_POST["future_market_price"];
				$futureList["qty"] = $_POST["future_qty"];
				
				$decisionList["option"] = $optionList;
				$decisionList["future"] = $futureList;
				
				$json["decision"] = $decisionList;*/

				
				
				$valueList = array();
				$valueList["name"] = $_POST["set_name"];
				$valueList["cutGain"] = $_POST["cut_gain"];
				$valueList["cutLoss"] = $_POST["cut_loss"];
				$valueList["trailStop"] = $_POST["trailstop"];
				$valueList["noOfSet"] = $_POST["set_num"];
				$valueList["id"] = 0;
				$valueList["decisionType"] = "OPEN";
				
				$orderSet = array();
				
											
				if (isset($_POST["future"]) && sizeof($_POST["future"])>0){
				foreach ($_POST["future"] as $future_id => $future_data){
					$orderList = array();
					$orderList["account"] = "D";
					$orderList["action"] = "ADD";
					$orderList["marketDataKey"] = array("source"=>"REAL_TIME", "product"=>$future_data["product"], "instrument"=>"FUTURE", 
					"type"=>"TICK", "contractMonth"=>$future_data["contractMonth"], "strikePrice"=>0, "periodLength"=>0, "contractCalendarMonth"=>0, "deciamFormat"=>"#");
					$orderList["position"] = $future_data["future_position"];
					$orderList["orderPrice"] = $future_data["future_market_price"];
					$orderList["orgOrderPrice"] =  $future_data["future_market_price"];
					$orderList["orderQty"] = $future_data["future_qty"];
					$orderList["orderType"] = "Limit";
					$orderList["commitAvgPrice"] = 0;
					$orderList["commitQty"] = 0;
					$orderList["status"] = "New";
					$orderList["workerId"] = -1;
					$orderList["wrapperNo"] = 0;
					$orderList["lastSentWrapperNo"] = -1;
					$orderList["ackWrapperNo"] = -1;
					$orderList["seqId"] = 0;
					$orderList["debugId"] = 5404432;
					$orderList["tPlus1"] = false;
					$orderList["orderTime"] = -1;
					$orderList["profitThreshold"] = 0;
					$orderList["lossThreshold"] = 0;
					$orderList["trailingStopRatio"] = 0;
					$orderList["trigger"] = isset($_POST["set_trigger"]) && $_POST["set_trigger"] == "Future".$future_id ? true : false;
					
					array_push($orderSet, $orderList);
				}
				}
				
				
				if (isset($_POST["option"]) && sizeof($_POST["option"])>0){
				foreach ($_POST["option"] as $opt_id => $opt_data){
					$orderList = array();
					$orderList["account"] = "D";
					$orderList["action"] = "ADD";
					$orderList["marketDataKey"] = array("source"=>"REAL_TIME", "product"=>$opt_data["product"], "instrument"=>$opt_data["instrument"], 
					"type"=>"PRICE", "contractMonth"=>$opt_data["contractMonth"], "strikePrice"=>$opt_data["option_strike"], "periodLength"=>0, "contractCalendarMonth"=>0, "deciamFormat"=>"#.##");
					$orderList["position"] = $opt_data["option_position"];
					$orderList["orderPrice"] = $opt_data["option_limit_price"];
					$orderList["orgOrderPrice"] =  $opt_data["option_limit_price"];
					$orderList["orderQty"] = $opt_data["option_qty"];
					$orderList["orderType"] = "Limit";
					$orderList["commitAvgPrice"] = 0.0;
					$orderList["commitQty"] = 0;
					$orderList["status"] = "New";
					$orderList["workerId"] = -1;
					$orderList["wrapperNo"] = 0;
					$orderList["lastSentWrapperNo"] = -1;
					$orderList["ackWrapperNo"] = -1;
					$orderList["seqId"] = 0;
					$orderList["debugId"] = 824586;
					$orderList["tPlus1"] = false;
					$orderList["orderTime"] = -1;
					$orderList["profitThreshold"] = 0;
					$orderList["lossThreshold"] = 0;
					$orderList["trailingStopRatio"] = 0;
					$orderList["trigger"] = isset($_POST["set_trigger"]) && $_POST["set_trigger"] == "Option".$opt_id ? true : false;
			
					
					array_push($orderSet, $orderList);
				}
				}
				
				$valueList["orderList"] = $orderSet;
				$json["value"] = str_replace("\"", "\"", json_encode($valueList));
				
				echo json_encode($json)."\n";
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json, false);	
				
			//	{"updTime":"1516726497213","decision_id":"1516726497213"} "
				
			
				
				var_dump($result);
			
				
				$response = json_decode($result, true);
				
			
				var_dump($result);
				
				$decision_id = $response["decision_id"];
				$_SESSION["decision_id"] = $decision_id;
				
				break;
				
				case "Combo Config":	
			
			
				$conn_r = array("host" => $host, "ip" => $ip, "port" => $port );
/*{"account":"veekoExtrema","action":"UPDATE_COMBO_CONFIG","value":"{\"name\":\"test combo\",\"cutGain\":500,\"cutLoss\":250,\"trailStop\":80,\"noOfSet\":5,\"id\":0,\"triggerPrice\":800.0,\"maxAffectDecisionIdx\":7,\"orderList\":[],\"decisionType\":\"OPEN\"}","sync":false}*/

	
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "UPDATE_COMBO_CONFIG";
				$json["sync"] = false;
						
				
				$valueList = array();
				$valueList["name"] = $_POST["set_name"];
				$valueList["cutGain"] = $_POST["cut_gain"];
				$valueList["cutLoss"] = $_POST["cut_loss"];
				$valueList["trailStop"] = $_POST["trailstop"];
				$valueList["noOfSet"] = $_POST["set_num"];
				$valueList["id"] = $_POST["decision_id"];
				$valueList["triggerPrice"] = $_POST["triggerprice"];
				$valueList["maxAffectDecisionIdx"] = $_POST["decisionidx"];
				$valueList["orderList"] = array();
				$valueList["decisionType"] = "OPEN";
				
				
				$json["value"] = str_replace("\"", "\"", json_encode($valueList));
				
			
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json, false);	
				
			//	{"status":"OK"}  "
				
				echo json_encode($json)."\n";
				
				
			
				var_dump($result);
				
				
				break;	
				
			case "Settle":	
			
				$conn_r = array("host" => $host, "ip" => $ip, "port" => $port );
				/*	account : 
					action FORCE_SETTLE
					value true
					sync false */
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "FORCE_SETTLE";
				$json["value"] = true;
				$json["sync"] = false;

					$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json, false);	
				
				
				echo json_encode($json)."\n";
				
				
			
				var_dump($result);
				
				
				break;
				
			default:
				break;
		}
			
	}
}

	
?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<?php 
$g_userid = "rickyTest";

?>
<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#FFF;
		max-width :470px;
		overflow-x:hidden;
}

.btnreset{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :120px;
	border : 3px solid #CCCCCC;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CCCCCC;
}

.btnforce, .btnopen{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #CCCCCC;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CCCCCC;
}

.btncall{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #00CC00;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#00CC00;
}

.btnput{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #CC0000;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CC0000;
}

.btnsettle{
	background-color : #007eff;
	border-radius:3px;
	height : 40px;
	width :85px;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:white;
	border : none;
}

.offset-left-30{
	margin-left:30px;
}

.switch-wrapper {
  display: inline-block;
  position: relative;
  top: 3px;
}

.switch-button-label {
    float: left;

    font-size: 10pt;
    cursor: pointer;
}

.switch-button-label.off {
    color: #adadad;
}

.switch-button-label.on {
    color: #0088CC;
}

.switch-button-background {
    float: left;
    position: relative;

    background: #ccc;
    border: 1px solid #aaa;

    margin: 1px 10px;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;

    cursor: pointer;
}

.switch-button-button {
    position: absolute;

    left: -1px;
    top : -1px;

    background: #FAFAFA;
    border: 1px solid #aaa;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

.set{
/*	padding-top:20px;*/
	height:50px;
}

.row .decisionidx ,.row .triggerprice{
	display : inline-block;
	padding-bottom:30px;
}

.cnttxt{ width:40px;}

.threstxt{ width:80px;}

.profile{ width:100%; min-width:850px; height:360px;}

.action_row{padding:10px;   width:100%;}
.action_row .left{float:left; width:500px;}
.action_row .right{float:right; width:300px;}
.decision_name{width:200px; height:30px;}
.clearfix{clear:both;}

.trade_row{  position: relative;
    bottom: 0; margin-bottom:10px;}

@media only screen and (max-device-width: 480px){
	.profile{ width:100%; width:400px; height:360px;}
}

.option_row, .future_row, .action_row{
	display:block;
	width:100%;
}
.option_row, .future_row{
	height:80px;
}

.option_row .strike, .option_row .limit_price,  .future_row .market_price{
	width:80px;
}	

.option_row .qty, .future_row .qty, .trade_row .cut_gain, .trade_row .cut_loss, .trade_row .trailstop,  .trade_row .num_set{
	width:50px;
}

.option_row h1, .future_row h1{
	font-size:16px;
	display : inline-block;
}

.position, .instrument, .product{
	margin-left:10px;
	display : inline-block;
	width:80px;
}

.settings{
	display : inline-block;
}

.block{
	max-width: 80px;
	padding: 0 10px;	
	display:inline-block;
}

label{cursor:pointer;}

</style>
  <body>
	<div class="container">
	
	<div class="profile">
		<ul>
		<?php foreach($setlist as $optid => $optrow){ 
			$setid =  isset($optrow["set_id"])?$optrow["set_id"]:"new"; 
		?>
			<li><a href="#tabs-<?php echo $setid; ?>"><?php echo $optrow["set_name"]; ?></a></li>
		<?php } ?>			
			<li><a href="#tabs-new">+</a></li>

		</ul>
		
	
	<?php 
	$option_arr = $setlist;
	
	// untitled index
	$uindex = 1;	
	foreach($option_arr as $optid => $optrow){
		if (stripos($optrow["set_name"], "Untitled-") !== false){
			$uindex = intval(str_replace("Untitled-", "", $optrow["set_name"]))+1;
		}
	}
	
	$option_arr["new"] = array("set_name" => "Untitled-".$uindex);
	foreach($option_arr as $optid => $optrow){ ?>
	<div id="tabs-<?php echo isset($optrow["set_id"])?$optrow["set_id"]:"new"; ?>">
	<form action="" method="POST" >
		<input type="hidden"  name="decision_id" value="<?php echo $decision_id; ?> " ></input>
		<div class="action_row">
			<div class="left">
			<input type="hidden" name="set_id" value="<?php echo isset($optrow["set_id"])?$optrow["set_id"]:0; ?>" />
			<input type="text" name="set_name" class="decision_name" value="<?php echo $optrow["set_name"]; ?>" />
			
			<div class="block">		
			Add Option 
			<input type="submit"  name="add_option_action" value="+" ></input>
			</div>
				
			<div class="block">	
			Add Future
			<input type="submit"  name="add_future_action" value="+" ></input>
			</div>
			
			</div>
			<div class="right">
			<input type="submit" class="btnsettle" name="set_action" value="Save" ></input>
			<input type="submit" class="btnsettle" name="set_action" value="Clear" ></input>
			<input type="submit" class="btnsettle" name="set_action" value="Remove" ></input>
			</div>
			<div class="clearfix"></div>
		</div>	
	
		<div class="set">
		
				<?php 
					
				
					
				
					
					/*$opt_trigger_checked =  isset($optrow["set_trigger"]) ? ($optrow["set_trigger"] == "Option" ? "checked" : "") : "";
					$future_trigger_checked =  isset($optrow["set_trigger"]) ? ($optrow["set_trigger"] == "Future" ? "checked" : "") : "";
					
					$opt_trigger_checked = ($opt_trigger_checked == "" && $future_trigger_checked == "") ? "checked" : $opt_trigger_checked;*/
					
					$set_id = isset($optrow["set_id"]) ? $optrow["set_id"] : 0;
								
					$criteria = array("strategy_set_id" => $set_id, "status"=>1);
					
					$option_list = $optionset->getOptionList($criteria);
					$future_list = $optionset->getFutureList($criteria);
					
					//var_dump($option_list, $future_list);
				
				?>

		
		<?php 
		
		foreach ($option_list as $option_id => $option_row){ ?>
			<div class="option_row">
				<?php 
					$settype1_checked = "selected";
					$settype2_checked = "";
					
					$optlong_checked = isset($option_row["option_position"]) ? ($option_row["option_position"] == "LONG" ? "checked" : "") : "";
					$optshort_checked = isset($option_row["option_position"]) ? ($option_row["option_position"] == "SHORT" ? "checked" : "") : "";
					$optlong_checked = ($optlong_checked == "" && $optshort_checked == "") ? "checked" : $optlong_checked;
					
					$optcall_checked = isset($option_row["instrument"]) ? ($option_row["instrument"] == "OPTION_CALL" ? "checked" : "") : "";
					$optput_checked = isset($option_row["instrument"]) ? ($option_row["instrument"] == "OPTION_PUT" ? "checked" : "") : "";
					$optcall_checked = ($optlong_checked == "" && $optput_checked == "") ? "checked" : $optlong_checked;
					
					
					$contractMonth1_selected = isset($option_row["contractMonth"]) ? ($option_row["contractMonth"] == "SPOT" ? "selected" : "") : "";
					$contractMonth2_selected = isset($option_row["contractMonth"]) ? ($option_row["contractMonth"] == "NEXT" ? "selected" : "") : "";
					$contractMonth1_selected = ($contractMonth1_selected == "" && $contractMonth2_selected == "") ? "selected" : $contractMonth1_selected;
					
					$product1_selected = isset($option_row["product"]) ? ($option_row["product"] == "MHI" ? "selected" : "") : "";
					$product2_selected = isset($option_row["product"]) ? ($option_row["product"] == "HSI" ? "selected" : "") : "";
					$product1_selected = ($product1_selected == "" && $product2_selected == "") ? "selected" : $product1_selected;
				?>
			
				<h1>
				<input type="hidden" name="option[<?php echo $option_id; ?>][optionId]" value="<?php echo $option_id; ?>"/>

				<?php /*<select name="settype" class="settype">
					<option value="Option" <?php echo $settype1_checked; ?> >Option</option>
					<option value="Future" <?php echo $settype2_checked; ?> >Future</option>
				</select> */ ?>	
				Option<br/>
				<select name="option[<?php echo $option_id; ?>][contractMonth]" class="contractMonth" onchange="">
					<option value="SPOT" <?php echo $contractMonth1_selected; ?> >SPOT</option>
					<option value="NEXT" <?php echo $contractMonth2_selected; ?> >NEXT</option>
				</select>
				</h1>
				<div class="position">
					<input type="radio" name="option[<?php echo $option_id; ?>][option_position]" value="LONG" <?php echo $optlong_checked; ?> id="optlong" /><label for="optlong">LONG</label> <br/><input type="radio" name="option[<?php echo $option_id; ?>][option_position]" value="SHORT" <?php echo $optshort_checked; ?> id="optshort" /><label for="optshort">SHORT</label>
				</div>
				<div class="instrument">
					<input type="radio" name="option[<?php echo $option_id; ?>][instrument]" value="OPTION_CALL" <?php echo $optcall_checked; ?> id="optioncall" /><label for="optioncall">CALL</label> <br/><input type="radio" name="option[<?php echo $option_id; ?>][instrument]" value="OPTION_PUT"  <?php echo $optput_checked; ?> id="optionput"/><label for="optionput">PUT</label>
				</div>
				<div class="product">
				<select name="option[<?php echo $option_id; ?>][product]" class="product" onchange="">
					<option value="MHI" <?php echo $product1_selected; ?> >MHI</option>
					<option value="HSI" <?php echo $product2_selected; ?> >HSI</option>
				</select>
				</div>
				<div class="settings">
				<div class="block">
				Strike <br><input class="strike" name="option[<?php echo $option_id; ?>][option_strike]" type="text" value="<?php echo isset($option_row["option_strike"])?$option_row["option_strike"]:0; ?>"></input></div>
				<div class="block">
				Qty <br> <input class="qty" name="option[<?php echo $option_id; ?>][option_qty]" type="text" value="<?php echo isset($option_row["option_qty"])?$option_row["option_qty"]:0; ?>"></input>
				</div>
				<div class="block">
				Limit Price <br> <input class="limit_price" name="option[<?php echo $option_id; ?>][option_limit_price]" type="text"  value="<?php echo isset($option_row["option_limit_price"])?$option_row["option_limit_price"]:0; ?>"></input></div>
				
				<div class="block">
				<?php
				$opt_trigger_checked =  isset($optrow["set_trigger"]) && $optrow["set_trigger"]=="Option".$option_id  ? "checked" : ""; ?>
				<input type="radio" name="set_trigger" id="triggeropt<?php echo $option_id; ?>" value="Option<?php echo $option_id; ?>" <?php echo $opt_trigger_checked; ?> /><label for="triggeropt<?php echo $option_id; ?>">Trigger</label>
				</div>
				<div class="block">		
					<button type="submit" name="del_option_action" value="<?php echo  $option_id; ?>">-</button>				
				</div>
				</div>
				
			</div>
		<?php } ?>	
			
		<?php 
		
		foreach ($future_list as $future_id => $future_row){ ?>
			<div class="future_row">
				<?php
					$settype1_checked = "";
					$settype2_checked = "selected";
					
					$futurelong_checked = isset($future_row["future_position"]) ? ($future_row["future_position"] == "LONG" ? "checked" : "") : "";
					$futureshort_checked = isset($future_row["future_position"]) ? ($future_row["future_position"] == "SHORT" ? "checked" : "") : "";					
					$futurelong_checked = ($futurelong_checked == "" && $futureshort_checked == "") ? "checked" : $futurelong_checked;
					
					
					$contractMonth1_selected = isset($future_row["contractMonth"]) ? ($future_row["contractMonth"] == "SPOT" ? "selected" : "") : "";
					$contractMonth2_selected = isset($future_row["contractMonth"]) ? ($future_row["contractMonth"] == "NEXT" ? "selected" : "") : "";
					$contractMonth1_selected = ($contractMonth1_selected == "" && $contractMonth2_selected == "") ? "selected" : $contractMonth1_selected;
					
					$product1_selected = isset($future_row["product"]) ? ($future_row["product"] == "MHI" ? "selected" : "") : "";
					$product2_selected = isset($future_row["product"]) ? ($future_row["product"] == "HSI" ? "selected" : "") : "";
					$product1_selected = ($product1_selected == "" && $product2_selected == "") ? "selected" : $product1_selected;
					
				?>
			
				<h1>
				<input type="hidden" name="future[<?php echo $future_id; ?>][futureId]" value="<?php echo $future_id; ?>"/>
				<?php /*<select name="settype" class="settype" onchange="">
					<option value="Option" <?php echo $settype1_checked; ?> >Option</option>
					<option value="Future" <?php echo $settype2_checked; ?> >Future</option>
				</select>	*/ ?>
				Future<br/>
				<select name="future[<?php echo $future_id; ?>][contractMonth]" class="contractMonth" onchange="">
					<option value="SPOT" <?php echo $contractMonth1_selected; ?> >SPOT</option>
					<option value="NEXT" <?php echo $contractMonth2_selected; ?> >NEXT</option>
				</select>
				</h1>
				<div class="position">
					<input type="radio" name="future[<?php echo $future_id; ?>][future_position]" value="LONG" <?php echo $futurelong_checked; ?> id="futurelong" /><label for="futurelong">LONG</label> <br/><input type="radio" name="future[<?php echo $future_id; ?>][future_position]" value="SHORT"  <?php echo $futureshort_checked; ?> id="futureshort"/><label for="futureshort">SHORT</label>
				</div>
				<div class="instrument"></div>
				<div class="product">
				<select name="future[<?php echo $future_id; ?>][product]" class="product" onchange="">
					<option value="MHI" <?php echo $product1_selected; ?> >MHI</option>
					<option value="HSI" <?php echo $product2_selected; ?> >HSI</option>
				</select>
				</div>
				<div class="settings">
					<div class="block">
				Market Price  <br/><input class="market_price" name="future[<?php echo $future_id; ?>][future_market_price]" type="text"  value="<?php echo isset($future_row["future_market_price"]) ? $future_row["future_market_price"] : 0; ?>"></input></div>
					<div class="block">
				Qty <br/> <input class="qty" name="future[<?php echo $future_id; ?>][future_qty]" type="text"  value="<?php echo isset($future_row["future_qty"])?$future_row["future_qty"]:0; ?>"></input></div>
				<div class="block" style="width:120px;"></div>
					<div class="block">
					<?php $opt_trigger_checked =  isset($optrow["set_trigger"]) && $optrow["set_trigger"]=="Future".$future_id  ? "checked" : ""; ?>
				<input type="radio" name="set_trigger" id="triggerfuture<?php echo $future_id; ?>" value="Future<?php echo $future_id; ?>" <?php echo $opt_trigger_checked; ?> /><label for="triggerfuture<?php echo $future_id; ?>">Trigger</label>
				</div>
				<div class="block">		
					<button type="submit" name="del_future_action" value="<?php echo  $future_id; ?>">-</button>				
				</div>
			</div>
			</div>
			
		<?php  } ?>
			<div class="row">
				
				<div class="decisionidx">
				<?php $decisionidx = ""; ?>
					Decision IDx: <input type="text" name="decisionidx" value="<?php echo isset($optrow["decisionidx"]) && $optrow["decisionidx"]>0 ?$optrow["decisionidx"]:0; ?>"  />
				</div>
				
				<div class="triggerprice">
					<?php $triggerprice = ""; ?>
					Trigger Price : <input type="text" name="triggerprice" value="<?php echo isset($optrow["triggerprice"]) && $optrow["triggerprice"]>0 ?$optrow["triggerprice"]:0; ?>"  />
				</div>			
			</div>
				
			<div class="trade_row">	
				
				Cut Gain <input type="text" name="cut_gain" class="cut_gain" value="<?php echo isset($optrow["cut_gain"])?$optrow["cut_gain"]:0; ?>"></input>
				Cut Loss <input type="text" name="cut_loss" class="cut_loss" value="<?php echo isset($optrow["cut_loss"])?$optrow["cut_loss"]:0; ?>"></input>
				TrailStop <input type="text" name="trailstop" class="trailstop" value="<?php echo isset($optrow["trailstop"])?$optrow["trailstop"]:0; ?>"></input> 
			
				No. of Set <input type="text" name="set_num" class="num_set" value="<?php echo isset($optrow["set_num"]) && $optrow["set_num"]>0 ?$optrow["set_num"]:1; ?>"></input>

			
			<input type="submit" class="btnopen" name="set_action" value="Open Order" ></input>
			
			
			<input type="submit" class="btnsettle" name="set_action" value="Combo Config" ></input>
			
					
			<input type="submit" class="btnsettle" name="set_action" value="Settle" ></input>
			</div>

		</div>
	</form>
	</div>	
	
	<?php } ?>
 </div>
  </body>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script src="js/jquery.switchButton.js"></script>
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
      $(function() {
        $(".switch-wrapper input").switchButton({
		  checked:false,
          width: 50,
          height: 20,
		  button_width: 20
        });


		$( ".profile" ).tabs({
		
		  beforeLoad: function( event, ui ) {
			ui.jqXHR.fail(function() {
			  ui.panel.html(
				"Couldn't load this tab. We'll try to fix this as soon as possible. " +
				"If this wouldn't be a demo." );
			});
		  }
		});
		
		<?php if (isset($_GET["set_id"]) && $_GET["set_id"] > 0){ ?>
			$('.profile a[href="#tabs-<?php echo $_GET["set_id"]; ?>"]').click();
		<?php } ?>	
		
	  });
		
  </script>
</html>