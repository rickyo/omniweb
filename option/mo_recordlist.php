<?php

ini_set("allow_url_fopen", 1);
include_once("config.php");
include_once("connection.php");


if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}


?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#343235;
	color:#FFF;
	max-width :470px;
	overflow-x:hidden;
}

.shortxt{
	width:50px;
}

.container{
	width:1000px;
}

.optfilter ul li{
	display :inline;
	padding-left:30px;
}

.recordtable{
	background:#5f5d60;
	width:100%;
	height:270px;
}

.recordtable table{
	width:100%;
}

.recordtable table thead th{
	color : #FFF;
	width:13%;
	padding:3px;
	background:#1e1b20;
	font-size:12px;
	
}
.recordtable tr td{
	padding : 10px;
	border-bottom : #CCC solid 1px;
}

.filter{
	display : block;
	
}
.set_code{
	width:200px;
}
</style>
  <body>
 
	<input type="hidden" name="userid" value="<?php echo $g_userid; ?>" />

	<input type="hidden" name="server" value="<?php echo $host; ?>" />
	<input type="hidden" name="port" value="<?php echo $port; ?>" />	
	
	<input type="hidden" name="market_close" value="-1" />

	<div class="container">
		<div class="title">Record</div>
		<div class="optfilter">
		<ul>
			<li>All</li>
			<li>Working</li>
			<li>Filled</li>
			<li>Cancelled</li>
		</ul>
		<div class="filter">
		Filter :
		<select name="">
			<option value="1">Stragety I</option>
			<option value="2">Stragety II</option>
			<option value="3">Stragety III</option>
			<option value="4">Stragety IV</option>
		</select>
		</div>
		</div>
		<div class="recordtable">
			 <table cellpadding="0" cellspacing="0" border="0">
			  <thead>
				<th>Set Code</th>
				<th>Strike</th>
				<th>Price</th>

				<th>P/L</th>
				<th>Qty</th>
				<th>Cut Gain</th>
				<th>Cut Loss</th>
				<th>TrailStop</th>
				<th>No. of set</th>
				<th>Action</th>

			</thead>
			 <tbody>
			 <tr>
						
						<td class="set_code">StrtegyI <br/>  LONG CALL HSI<br><br> 002864 SHORT HSI<br>2018-01-14 15:12:12</td>
						<td>30133</td>
						<td>142 <br> 30100</td>
						<td>+30</td>
						<td>4 <br/> 4</td>
						<td><input type="text" class="shortxt">50</input></td>
						<td><input type="text" class="shortxt">40</input></td>
						<td><input type="text" class="shortxt">80</input>%</td>
						<td>2</td>
						<td> 	
						<input type="submit" class="btnreset" name="open_order" value="Split" ></input>
						<input type="submit" class="btnsettle" name="Settle" value="Settle" >
			</td>
					</tr>	
			 <?php 
			 	
			$conn_r = array(
								"host" => $host,
								"ip" => gethostbyname($host),
								"port" =>$port
							);
			
			 
			 	$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "OPEN_ORDER";
				$json["value"] = "TRUE";
				
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$open_record = json_decode($result);
				}
				
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "PENDING_ORDER";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$pending_record = json_decode($result);
				}
				
				$json = array();
				$json["account"] = $g_userid;
				$json["action"] = "SETTLED_ORDER";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if ($result != "")
				{
					$settled_record = json_decode($result);
				}
				
				
				
				if (isset($open_record) && sizeof($open_record) > 0){
				foreach ($open_record as $rec){ 
					$recordinfo = $rec->orderList[0];
					 ?>
					<tr>
						<?php
							$price = $recordinfo->commitAvgPrice;
							if ($price == 0){
								$price = $recordinfo->orderPrice;
							}
							
							
						?>
						<td><?php echo $recordinfo->id; ?> <br/> <?php echo date("Y-m-d H:i", $recordinfo->orderTime/ 1000 + $g_gmt*60*60); ?></td>
						<td><?php echo $recordinfo->position; ?> <?php echo $price; ?></td>
						<td></td>
						<td><?php echo $recordinfo->profitThreshold; ?></td>
						<td><?php echo $recordinfo->lossThreshold; ?></td>
						<td><?php echo $recordinfo->trailingStopRatio; ?></td>
						<td> Open</td>
					</tr>	
					
	<?php		}
			   }
			   
			  
				if (sizeof($settled_record) > 0){
				foreach ($settled_record as $rec){ 
					$recordinfo = $rec->orderList[0];
				?>
					<tr>
						<?php
							$price = $recordinfo->commitAvgPrice;
							if ($price == 0){
								$price = $recordinfo->orderPrice;
							}
						?>
						<td><?php echo $recordinfo->id; ?> <br/> <?php echo date("Y-m-d H:i", $recordinfo->orderTime/ 1000 + $g_gmt*60*60); ?></td>
						<td><?php echo $recordinfo->position; ?> <?php echo $price; ?></td>
						<td></td>
						<td><?php echo $recordinfo->profitThreshold; ?></td>
						<td><?php echo $recordinfo->lossThreshold; ?></td>
						<td><?php echo $recordinfo->trailingStopRatio; ?></td>
						<td> Settled</td>
					</tr>	
					
	<?php		}
			   }
			 
			 ?>
			 
			 
			 </tbody>
			 </table>
		
		</div>
	
	
	
	</div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	
    <script>
      $(function() {
		  
		 var url = "OmniController.php";
  	 	 var market_close = 0;
		 var refresh_rate = <?php echo refresh_rate(15); ?>
		 
		 setInterval(function () {getRecordList()}, refresh_rate);//request every x seconds
		 
		 function getOpenOrder(){
				// balance
				var data = {};
				data.action = "open_order";
				data.host = server;
				data.port = port;
				data.account = userid;
				data.value = "TRUE";
				
				
				$.post(
					url,
					data,
					function(json) {
						
						//	console.log(json);
						}
					
				);
		}
		 
		function getRecordList(){
		}
			
		 
	  });
	 </script> 
  </body>
</html>