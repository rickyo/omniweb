<?php

ini_set("allow_url_fopen", 1);

include_once("configAdmin.php");
include_once("connection.php");


if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniTrader WebApp Admin</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	 <link rel="stylesheet" href="./css/admin.css?v=<?php echo date("YmdHis"); ?>">
<link rel="stylesheet" href="alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />

	 
</head>

<body>

<?php

	$conn_r = array("host" => $host, "ip" => $ip, "port" => $port);	
	$json = array();
	$json["account"] = $g_userid;
	$json["action"] = "MARKET_DATA";
	$json["value"] = "TRUE";


	try {
	   
		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		
		if ($result != ""){
			$market_data = json_decode($result);
		}
		$close_net = $market_data ? $market_data->close - $market_data->prevClose : "-";
		
		$param = "userid=".$g_userid."&host=".$host."&port=".$port; 
		
	} catch (Exception $e) {
		echo "<p class=\"error\">ERROR - Connection Failure </p>";
	}
	
	
	
?>

<style>
body {width:480px;} 
.title {color:#000000; text-transform: uppercase; background:#b8671d;  padding:5px;}
.box {display:block; width:80%; margin : 0 15px;padding: 10px 0; }
#marketinfo {  height:150px; }
.align-left {    vertical-align: top;}
.col-50{ height:50px;}
.contentbox{background:#1b1405; border:1px solid #b8671d; }
.container{padding:10px;}
#connprofile span{color:#FFFFFF; display:inline-block;}
#connprofile input{color:#FFFFFF; background:#1b1405; border: 1px solid #CCCCCC;}
button {   -webkit-appearance: button; overflow: visible;  text-transform: none; font: inherit;} 

</style>

	<header>
	<div class="box col-50">
		<div class="logo"><img src="images/logo.jpg" height="50"> </div>
		<h1>Omni Admin Panel</h1>
	</div>
	</header>
	<div id="marketinfo" class="box ">
			<div class="contentbox" id="marketheader">
			
			<div class="title"> Admin Panel </div>
			<div class="container">
			
			<div class="align-left" >
				<div class="nQ" ><b>MHI</b> <span class="netQty current <?php echo isset($close_net) && $close_net > 0 ? "green" : "red"; ?>"><?php echo isset($market_data) &&  $market_data ?  $market_data->netQty : "-"; ?> </span></div>
				
				<div class="current <?php echo isset($close_net) && $close_net > 0 ? "green" : "red"; ?>"><span class="market_price"><?php echo isset($market_data) && $market_data ? $market_data->close : "-";?></span>  <span class="market_price_net"><?php echo isset($close_net)? ( $close_net > 0 ?  "+".$close_net : $close_net) : "-"; ?></span></div>
			</div>
			
			
			<div class="align-right">
						<table class="market_info" cellspacing="0" cellpadding="0" border="0">
						 <tr>
							<td width="50%">
							  <div class="high" > H <?php echo isset($market_data) &&$market_data ? ceil($market_data->high) : "-"; ?></div> 
							  <div class="low" > L <?php echo isset($market_data) &&$market_data ? ceil($market_data->low) : "-"; ?></div>
							 </td>
							<td width="50%" class="offset-left-30">
							  <div class="open" > O <?php echo isset($market_data) &&$market_data ? ceil($market_data->open) : "-"; ?></div>				 
							  <div class="ppThreshold" > T <?php echo isset($market_data) &&$market_data ? $market_data->ppThreshold : "-" ; ?> <?php echo isset($market_data) &&$market_data ? $market_data->ppDirection : "";?></div>
							  
							 </td>
						</tr>
						</table>
					 <?php if (isset($market_data) && $market_data){ ?>	
					  <div class="vwap" >VWAP <?php echo $market_data->vwap;?> <?php echo $market_data->vwapDiff > 0? "+".$market_data->vwapDiff : $market_data->vwapDiff; ?></div>
					 <?php } ?> 
					 <input type="hidden" name="vwapvalue" class="vwapvalue" value="0" />
					 
					 
			

			</div>	
						<div class="last_updated"><?php echo isset($market_data->lastUpdate) && $market_data->lastUpdate ?date("Y-m-d H:i:s",  $market_data->lastUpdate/ 1000 ): "-";?></div>

			</div>
			<div class="clearfix"></div>
		 </div>	
		</div>	
		<div class="box col-50">
		<div class="align-left" >
		 <form method="GET" action=""  id="connprofile">
			<span>User</span> <input type="text" name="userid" class="usertext" value="<?php echo $g_userid; ?>"></input>
			<span>Host</span> <input type="text" name="host" class="host" value="<?php echo $host; ?>"></input>
			<span>Port</span> <input type="text" name="port" class="port" value="<?php echo $port; ?>"></input>
			<input type="submit" value="Submit" />
		 </form>
		 </div>
		<div class="align-right" >
		 	<div class="adminpanel">
			
				<button type="submit" id="cp_refresh_mkt_data" value="refresh_mkt_data"> Refresh Market Data </button>
			</div>
		 </div>
		</div>


	<div class="clearfix"></div>



	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
	<script src="alertify/lib/alertify.min.js"></script>

    <script>
		var refresh_rate = <?php echo refresh_rate(2);?>; 
		var refresh_rate_vwap = <?php echo refresh_rate(10);?>; 
		
		var vwapcheck = 0;
		var vwapelapsed = 0;
		var vwaptime_threshold = 30; // check vwap no respond after 30 sec
		
		
		$('document').ready(function () {
			
			var url = "OmniControllerAdmin.php"; 		
			
		setInterval(function () {getMarketData()}, refresh_rate);//request every x seconds
		setInterval(function () {checkVwapHeartBeat()}, refresh_rate_vwap);//request every x seconds
		
		
		function checkVwapHeartBeat(){
			

			var vwapvalue = parseFloat($("#marketheader .vwapvalue").val())
			console.log("Tracking vwap ... " + vwapvalue + " Elaspsed time: "+ vwapelapsed);
			
			if (vwapcheck == vwapvalue){
				vwapelapsed += refresh_rate_vwap/1000;
				if (vwapelapsed >= vwaptime_threshold){
					alertify.error("Warning : VWAP Stopped <br>("+vwapelapsed +" sec)");
				}
			}else{
				vwapelapsed = 0;
				vwapcheck = vwapvalue;
			}
				
		}
		
		function getMarketData() {
			
				var accframe = $('#omni_account').contents();
			
				var recframe = $('.omni_record').contents();
				console.log("MK"+$(accframe).find('.account_list input[name=market_close]').val());
			 
			 	var url = "OmniControllerAdmin.php";

				var data = {};
				data.action = "market_data";
				data.host = "<?php echo $host;?>";
				data.ip = "<?php echo $ip;?>";
				data.port = "<?php echo $port;?>";
				data.userid = "<?php echo $g_userid;?>";
				$.post(
					url,
					data,
					function(json) {
						
					console.log(json.lastupdate);
						if (json.ok == 1){
						
							var price_net = json.close_net;
							if (json.close_net > 0){
								$("#marketheader .current").removeClass("red");
								$("#marketheader .current").addClass("green");
								price_net = "+"+json.close_net;
							}else{		
								$("#marketheader .current").removeClass("green");
								$("#marketheader .current").addClass("red");
							}
							$("#marketheader .netQty").text(json.netQty);
							$("#marketheader .market_price").text(json.close);
							$("#marketheader .market_price_net").text(price_net);
							
							$("#marketheader .market_info .high").text("H " +json.high);
							$("#marketheader .market_info .low").text("L "+json.low);
							$("#marketheader .market_info .ppThreshold").text("T "+json.ppThreshold + " "+ json.ppDirection);
							$("#marketheader .market_info .open").text("O "+json.open);
							
							var vdiff = json.vwapDiff;							
							if (vdiff > 0){vdiff = "+"+vdiff};
							$("#marketheader .vwap").text("VWAP "+json.vwap+" "+vdiff);
							//$("#marketheader .vwapvalue").val(parseInt(json.vwap));
							//veeko added
							$("#marketheader .vwapvalue").val(json.vwapraw);
							$("#marketheader .last_updated").text(json.lastupdate);
							
							console.log(json.elapsed_time);
							console.log(json.now);
							console.log(json.diff);
							if (json.elapsed_time > 0){
								alertify.error("Warning : Last Update Timer Stopped <br>("+json.elapsed_time +"min)");
							}
							
							$(recframe).find('input[name=market_close]').val(json.close);
							$(accframe).find('.account_list input[name=market_close]').val(json.close);
							//console.log($(accframe).find('.account_list input[name=market_close]').val());
												
							
							return 1;
						}

				}).fail( function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
    //alert(textStatus);
				});
				
				
				
			/*	$(accframe).find('#acc_list').change(function(){
				
					var userid = $(this).val();
					
				
					$(accframe).find('.account_list .row').each(function(){
						
						
						$(this).removeClass("parent");
						
						if ($(this).find("input[name=userid]").val().trim() == userid){
												
												
						
							var this_server = $(this).find("input[name=server]").val();		
							var this_port = $(this).find("input[name=port]").val();

							$(recframe).find("input[name=userid]").val(userid);
							$(recframe).find("input[name=server]").val(this_server);
							$(recframe).find("input[name=port]").val(this_port);
							
							var rsrc = "recordlist.php?userid="+userid+"&host="+this_server+"&port="+this_port;
							 $('#recordlist').attr("src", rsrc);
							
							
							$(this).addClass("parent");
						}
					});
				 
				}); */
				
			}

		
		$("#cp_refresh_mkt_data").click(function(){
							
				var data = {};
				data.action = "reload_mkt_data";
				data.host = "<?php echo isset($host) && $host ? $host : ""; ?>";
				data.ip = "<?php echo isset($ip) && $ip ? $ip : ""; ?>";
				data.port = "<?php echo isset($port) && $port ? $port : ""; ?>";
				data.userid = "<?php echo isset($g_userid) && $g_userid ? $g_userid : ""; ?>";			
				data.value = "TRUE";
				
				$.post(
					url,
					data,
					function(json) {
						
						if (json.ok == 1){
							alert("Market data reloaded");
						}
					});
				
		});
		
		});
		
		 
		 
		 
	</script> 

</body>
</html>