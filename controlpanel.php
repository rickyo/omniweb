<?php

ini_set("allow_url_fopen", 1);

include_once("connection.php");


?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniAccount List</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

</head>

<?php 
$g_userid = "rickyTest";

?>
<style>

body{
	margin:0;
	padding:0;
	font-family : Arial;
	background-color:#343235;
	color:#FFF;
	max-width :470px;
	overflow-x:hidden;
}

.btnreset{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #CCCCCC;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CCCCCC;
}

.btnforce, .btnopen{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #CCCCCC;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CCCCCC;
}

.btncall{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #00CC00;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#00CC00;
}

.btnput{
	background-color : #343235;
	border-radius:3px;
	height : 40px;
	width :85px;
	border : 3px solid #CC0000;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:#CC0000;
}

.btnsettle{
	background-color : #007eff;
	border-radius:3px;
	height : 40px;
	width :85px;
	padding:10px;
	line-height:1px;
	cursor:pointer;
	color:white;
	border : none;
}

.offset-left-30{
	margin-left:30px;
}

.switch-wrapper {
  display: inline-block;
  position: relative;
  top: 3px;
}

.switch-button-label {
    float: left;

    font-size: 10pt;
    cursor: pointer;
}

.switch-button-label.off {
    color: #adadad;
}

.switch-button-label.on {
    color: #0088CC;
}

.switch-button-background {
    float: left;
    position: relative;

    background: #ccc;
    border: 1px solid #aaa;

    margin: 1px 10px;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;

    cursor: pointer;
}

.switch-button-button {
    position: absolute;

    left: -1px;
    top : -1px;

    background: #FAFAFA;
    border: 1px solid #aaa;

    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

.set{
	height:50px;
}

.cnttxt{ width:40px;}

.threstxt{ width:80px;}

</style>
  <body>
	<div class="container">
	
	<div class="profile">
		<ul>
			<li>Total Assets</li>
			<li>Floating P/L</li>
			<li>Total P/L</li>

		</ul>
	</div>
	<div class="set">
	
	 Algo
	 <div class="switch-wrapper">
		 <input type="checkbox" value="1"/>
	</div>	
	Sync
	 <div class="switch-wrapper">
		 <input type="checkbox" value="1"/>
	</div>
   </div>	
	<div class="set">
		Count <input type="text" class="cnttxt" value="1"/>

		<input type="submit"  class="btnreset" value="Reset" />
	</div>	
	<div class="set">
		<input type="submit"  class="btnforce" value="Force" />	
		<input type="submit" class="btnopen" value="Open" />

		<input type="submit" class="btncall offset-left-30" name="Call" value="Call" ></input>
		<input type="submit" class="btnput" name="Put" value="Put" ></input>
	</div>		
	<div class="set"> 
		Threshold <input type="text" class="threstxt"/>
		
				
		<input type="submit" class="btnsettle" name="Settle" value="Settle" ></input>
	 </div>	
	</div>
 
  </body>
  <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
    <script src="js/jquery.switchButton.js"></script>

    <script>
      $(function() {
        $(".switch-wrapper input").switchButton({
		  checked:false,
          width: 50,
          height: 20,
		  button_width: 20
        });
		
	  });
		
  </script>
</html>