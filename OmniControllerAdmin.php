<?php
header('Content-Type: application/json; charset=utf-8');

	include_once('configAdmin.php');
	include_once("connection.php");
	include_once("txnRecord.class.php");



	$json = array();
	
	if (_DEBUG){
		$_POST["host"]= $debug_host; 
	}
	
/*	$_POST["ip"] = gethostbyname($_POST["host"]);
	$_POST["port"] = 5987;
	$_POST["userid"] = "rickyTest";
	$_POST["action"] = "market_data";*/
	

	
	$json_ok = 0;
	
	

	$data = $_POST;
	
	

	

	$conn_r = array();
	if (isset($data["host"]) && isset($data["port"])){
		$conn_r = array(
								"host" => $data["host"],
								"ip" => gethostbyname($data["host"]),
								"port" => $data["port"]
							);
	}

	
	
	function getSessionTime($this_date){
	
	
		// night special handle
		$h = intval(date("H",strtotime($this_date))); 		
		$current_date = $this_date;
		if ($h >= 0 && $h < 1){
			$current_date = date("Y-m-d", strtotime("-1 day")); // current previous day
		}
		$session_time = array(
					// morning session
						array(
							"fr" =>$this_date." 09:15:00",
							"to" =>$this_date." 12:00:00"
						),
					// noon session	
						array(
							"fr" =>$this_date." 13:00:00",
							"to" =>$this_date." 16:30:00"
						),
					// night session	
						array(
							"fr" =>$current_date." 17:15:00",
							"to" =>$this_date." 00:59:00"
						)
		);
		
		return $session_time;
	}	
	
	function isTradingPeriod(){
	
		$inputTime = date("Y-m-d H:i:s");
		$session_time = getSessionTime(date("Y-m-d", strtotime($inputTime)));			


		foreach ($session_time as $i => $t)
		{
						
			$fromTime = strtotime($t["fr"]);
			$toTime = strtotime($t["to"]);
			
			
		
			$flag = (strtotime($inputTime) >= $fromTime && strtotime($inputTime) <= $toTime);
			if ($flag){
				return true;
			}
		}
		
		
		return false;
	}
	
	
	
	// market data
	if (isset($data["action"]) && $data["action"]=="market_data")
	{
	
		$conn_r = array("host" => $data["host"], "ip" => $data["ip"], "port" => $data["port"]);

		$json = array();
		$json["account"] = $data["userid"];
		$json["action"] = "MARKET_DATA";
		$json["value"] = "TRUE";

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != ""){
			$market_data = json_decode($result);
			
			$close_net = $market_data->close - $market_data->prevClose;

			$json["close_net"] = $close_net;
			$json["high"] = $market_data->high;
			$json["low"] = $market_data->low;		
			$json["open"] = $market_data->open;
			$json["close"] = $market_data->close;
			$json["netQty"] = $market_data->netQty;
			$json["ppThreshold"] = $market_data->ppThreshold;		
			$json["ppDirection"] = $market_data->ppDirection;
			$json["vwap"]=intval($market_data->vwap);
			//veeko added
			$json["vwapraw"]=$market_data->vwap;
			$json["vwapDiff"]=intval($market_data->vwapDiff);
			$json["lastupdate"]= $market_data->lastUpdate ?date("Y-m-d H:i:s",  $market_data->lastUpdate/ 1000 ): "-";
			
			$json["elapsed_time"] = -1;
			$elapsed_time = intval((strtotime("now") - $market_data->lastUpdate/1000)/60);
			if (isTradingPeriod() ){
				$json["elapsed_time"] = $elapsed_time;				
			}
			
			$json["now"] = strtotime("now");
			$json["diff"] = $market_data->lastUpdate/1000;
			
			$json_ok = 1;
		}
		
		
	

	}
	
	// force open
	if (isset($data["action"]) && $data["action"]=="force_open")
	{
		$json = array();
		$json["account"] = $data["userid"];
		$json["action"] = "FORCE_OPEN";
		$json["value"] = $data["value"];
		$json["sync"] = $data["sync"];

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		/*
	
	//call
		account : 
	action FORCE_OPEN
	value LONG
	sync false
	
	// put
	account : 
	action FORCE_OPEN
	value SHORT
	sync false*/
	
	}
	
	// force settle
	if (isset($data["action"]) && $data["action"]=="force_settle")
	{
		/*	account : 
			action FORCE_SETTLE
			value true
			sync false */
		$json = array();
		$json["account"] = $data["userid"];
		$json["action"] = "FORCE_SETTLE";
		$json["value"] = $data["value"];
		$json["sync"] = $data["sync"];

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
	}
	
	// force position
	if (isset($data["action"]) && $data["action"]=="force_position")
	{
		/*	// reset
		account : 
	action FORCE_POSITION
	value NULL
	sync false
	
		account : 
	action OPEN_POSITION
	value -1
	sync false
	
			account : 
	action REVERSE
	value FALSE
	sync false */
		$json = array();
		$json["account"] = $data["userid"];
		$json["action"] = "FORCE_POSITION";
		$json["value"] = $data["value"];
		$json["sync"] = $data["sync"];

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
	}
	
	// remote configuration
	if (isset($data["action"]) && $data["action"]=="remote_configuration")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "REMOTE_CONFIGURATION";
		$json["value"] = $data["value"];
		
		$remote_config = array();
		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$remote_config = json_decode($result);
			$json["forcePosition"] = $remote_config->forcePosition;
			$json["forceOpen"] = $remote_config->forceOpen;
			$json["forceSettle"] = $remote_config->forceSettle;
			$json["breakPt"] = $remote_config->breakPt;
			$json["reverse"] = $remote_config->reverse;
			
			$algo_on = 0;
			if ($remote_config){
				if ($remote_config->forcePosition == "LONG" || $remote_config->forcePosition == "SHORT"){
					$algo_on = 1;
				}else if ($remote_config->forcePosition == "NIL"){
					$algo_on = 0;
				}else{
					$algo_on = 1;
				}
			}
			
			$json["algo_on"] = $algo_on;
											
			/*["forceSettle"]=> string(5) "false" ["breakPt"]=> string(2) "-1" ["reverse"]=> string(5) "false" ["forcePosition"]=> string(0) "" ["forceOpen"]=> string(5) "false" 		
				//	echo $result->forceOpen;
				/*	$remote_config->forceSettle;
					$remote_config->breakPt;
					$remote_config->reverse;
					$remote_config->forcePosition;				
					$remote_config->forceOpen;*/
			$json_ok = 1;
		}
		
	}
	
	// active order
	if (isset($data["action"]) && $data["action"]=="active_order")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "ACTIVE_ORDER";
		$json["value"] = $data["value"];

		
		
		
		$active_order = array();
		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		

		if (trim($result) != "")
		{			
			
			$active_order = json_decode($result);
		
			if ($active_order){
				$json["commitAvgPrice"] = $active_order->commitAvgPrice;
				$json["position"] = $active_order->position;
				$json["product"] = $active_order->marketDataKey->product;
				$json_ok = 1;
			}
		}
		
	}
	
	// balance
	if (isset($data["action"]) && $data["action"]=="balance")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "BALANCE";
		$json["value"] = $data["value"];

		$balance = array();
		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$json["balance"] = json_decode($result);
			$json_ok = 1;
		}
		
	}
	
	// algo status
	if (isset($data["action"]) && $data["action"]=="algo_status")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "ALGO_STATUS";
		$json["value"] = $data["value"];
		$json["sync"] = $data["sync"];

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	

		$json["status"] = trim($result);
		$json_ok = 1;
		
		
	}
	
	//reset
	if (isset($data["action"]) && $data["action"]=="reset")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "FORCE_POSITION";
		$json["value"] = "NULL";
		$json["sync"] = $data["sync"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		
		
		
		$json["account"] = $data["userid"];
		$json["action"] = "OPEN_POSITION";
		$json["value"] = "-1";
		$json["sync"] = $data["sync"];

		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		
		$json["account"] = $data["userid"];
		$json["action"] = "REVERSE";
		$json["value"] = "FALSE";
		$json["sync"] = $data["sync"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	

		$json["status"] = $result;
		$json_ok = 1;
		
		
	}
	
	
	//open order
	if (isset($data["action"]) && $data["action"]=="open_order")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "OPEN_ORDER";
		$json["value"] = $data["value"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$json["open_order"] = json_decode($result);
			$json_ok = 1;
		}
		
		
	}
	
	//pending order
	if (isset($data["action"]) && $data["action"]=="pending_order")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "PENDING_ORDER";
		$json["value"] = $data["value"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$json["pending_order"] = json_decode($result);
			$json_ok = 1;
		}
		
		
	}
	
	//settle order
	if (isset($data["action"]) && $data["action"]=="settled_order")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "SETTLED_ORDER";
		$json["value"] = $data["value"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$json["settled_order"] = json_decode($result);
			$json_ok = 1;
		}
		
		
	}
	
	//RELOAD_MKT_DATA
	if (isset($data["action"]) && $data["action"]=="reload_mkt_data")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "RELOAD_MKT_DATA";
		$json["value"] = $data["value"];

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{					
			$json["reload_mkt_data"] = json_decode($result);
			$json_ok = 1;
		}
		
		
	}
	
	// order statistics
	if (isset($data["action"]) && $data["action"]=="order_statistics")
	{
		$json["account"] = $data["userid"];
		$json["action"] = "ORDER_STATISTICS";
		$json["value"] = $data["value"];
		$json["sync"] = $data["sync"];
		
		
		$order_stats = array();
		$buycount = 0;
		$sellcount = 0;
		$productCde = "-";
	

		$connection = new Connection();		
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		if (trim($result) != "")
		{		
			$order_stats = json_decode($result);
			$order_stats = json_decode($order_stats);
			foreach ($order_stats as $stats){
						$buycount += $stats->buyCnt;
						$sellcount += $stats->sellCnt;
						$productCde = $stats->prodCde;
			}
			$json["ordertext"] = $productCde . " | Buy : ". $buycount. " | Sell : ". $sellcount;;
			$json_ok = 1;
		}
		
		
	}
	
	// ssve record
	if (isset($data["action"]) && $data["action"]=="save_record")
	{
	
			//open order
				$open_record = array();
				$pending_record = array();
				$settled_record = array();
		
				$json = array();
				$json["account"] =  $data["userid"];
				$json["action"] = "OPEN_ORDER";
				$json["value"] = "TRUE";
			
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if (trim($result) != "")
				{
					$open_record = json_decode($result);
				}
				
			// pending order	
				$json = array();
				$json["account"] = $data["userid"];
				$json["action"] = "PENDING_ORDER";
				$json["value"] = "TRUE";
	
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if (trim($result) != "")
				{
					$pending_record = json_decode($result);
				}
				
			// settle order	
				$json = array();
				$json["account"] = $data["userid"];
				$json["action"] = "SETTLED_ORDER";
				$json["value"] = "TRUE";
				
				$connection = new Connection();
				$connection->setconnect($conn_r);
				$result =  $connection->sendcommand($json);	
				if (trim($result) != "")
				{
					$settled_record = json_decode($result);
				}
				
				if (sizeof($open_record) > 0){
				foreach ($open_record as $rec){ 
					$recordinfo = $rec->orderList[0];
				
					$price = $recordinfo->commitAvgPrice;
					if ($price == 0){
						$price = $recordinfo->orderPrice;
					}
				}
				}
				
				// txn recordinfo
				
				$txnrec = new TxnRecord($registry);
				$txn_result = $txnrec->getRecord(array("orderTime"=>$recordinfo->orderTime));
				
				
	   
							
		
	}
	
	
	
	


	$json["ok"] = $json_ok;
	echo json_encode($json);
	exit;








?>

