
var url = "OmniController.php";
         
class vwapGauge{ 
    constructor(height, width) {
        this.vwap = 0;
        this.market_price = 0;
        this.ppThreshold = 0;
        this.ppState = '';
        this.vwap_scale_max = 3000;
    }

    getData(marketinfo){
        this.vwap = marketinfo.vwap;
        this.market_price = marketinfo.nominal;
        this.ppThreshold = marketinfo.ppThreshold;
        this.ppState = marketinfo.ppThresholdState;
        console.log("[getData] vwap="+this.vwap+" | market_price="+this.market_price+"| ppThreshold="+this.ppThreshold+"| ppState="+this.ppState);
    }

    vwapscale(orival){
          var value = parseInt((orival - (this.vwap - 230)) * this.vwap_scale_max / 460);
                //console.log("value= " + orival + " | converted ="+ value);
          return value > 0 ? (value > this.vwap_scale_max ? this.vwap_scale_max : value) : 0;

    }


    show(){

            var vwap = parseInt(this.vwap);
            var market_price = parseInt(this.market_price);
            var ppThreshold = parseInt(this.ppThreshold);
            var ppState = this.ppState;


         /*   if (vwap <= 0 || isNaN(vwap)){
                //console.log(vwap);
                $("#vwapgauge").hide();
                return;
            }else{
                $("#vwapgauge").show();
            }*/

            var offset = 0;
            var interval = [vwap-230+offset, vwap-200+offset, vwap-100+offset, vwap-2+offset, vwap+2+offset, vwap+100+offset, vwap+200+offset, vwap+230+offset];
            var intcolor = ["#F03E3E", "#FFDD00", "#30B32D", "#0000FF",  "#30B32D", "#FFDD00", "#F03E3E"];
            var pindex = interval.length-1;
            //check ppState
            var ppSize = 3;
            if(ppState.indexOf("*")>0){
                ppSize = 10;
            }
            var ppColor = "orange";
            if(ppState.indexOf("R")>=0){
                ppColor = "Purple";
            }
            for (var i=0; i < interval.length; i++){
                if (ppThreshold < interval[i]){
                    
                    
                    if (i > 0){
                        var min = ppThreshold-ppSize;
                        var max = ppThreshold+ppSize;
                        if(min <= interval[i-1]){
                            min = interval[i-1]+ppSize;
                        }
                        if(max >=interval[i]){
                            max = interval[i]-ppSize;
                        }
                        //interval.splice(i,0,ppThreshold-ppSize, ppThreshold+ppSize);
                        interval.splice(i,0,min, max);
                        intcolor.splice(i,0, ppColor, intcolor[i-1]);
                    }else{
                        interval.splice(i,0, ppThreshold-ppSize, ppThreshold+ppSize);
                        intcolor.splice(i,0, ppColor, intcolor[i]);
                    }
                    pindex = i;
                    break;
                }
            }
            if (pindex == interval.length-1){ // threshold last
                interval.push(ppThreshold-2);
                interval.push(ppThreshold+2);
                intcolor.push(ppColor);
                intcolor.push(intcolor[pindex-1]);
            }
            //fix vwap guage display problem
            interval.sort(function(a, b) {
              return a - b;
            });


            var zones = [];

            for (var i=0; i < interval.length; i++){
                var obj = {};
                obj.strokeStyle = intcolor[i];
                obj.min = this.vwapscale(interval[i]);
                obj.max = this.vwapscale(interval[i+1]);
                zones.push(obj);
            }

            //console.log(zones);
            /*
           {strokeStyle: "#F03E3E", min: vwapscale(vwap-230), max: vwapscale(vwap-200)}, // Red from 100 to 130
           {strokeStyle: "#FFDD00", min: vwapscale(vwap-200), max: vwapscale(vwap-100)}, // Yellow
           {strokeStyle: "#30B32D", min: vwapscale(vwap-100), max: vwapscale(vwap-1)}, // Green
           {strokeStyle: "#0000FF", min: vwapscale(vwap-1), max: vwapscale(vwap+1)}, // White
           {strokeStyle: "#30B32D", min: vwapscale(vwap+1), max: vwapscale(vwap+100)}, // Green
           {strokeStyle: "#FFDD00", min: vwapscale(vwap+100), max: vwapscale(vwap+200)}, // Yellow
           {strokeStyle: "#F03E3E", min: vwapscale(vwap+200), max: vwapscale(vwap+230)}  // Red

    */

            var opts = {
              angle: -0.04, // The span of the gauge arc
              lineWidth: 0.2, // The line thickness
              radiusScale: 0.83, // Relative radius
              pointer: {
                length: 0.6, // // Relative to gauge radius
                strokeWidth: 0.046, // The thickness
                color: '#FFFFFF' // Fill color
              },
              limitMax: false,     // If false, max value increases automatically if value > maxValue
              limitMin: false,     // If true, the min value of the gauge will be fixed

              strokeColor: '#E0E0E0',  // to see which ones work best for you
              generateGradient: true,
              highDpiSupport: true,     // High resolution support
              staticZones: zones,
              staticLabels: {
                  font: "10px sans-serif",  // Specifies font
                  labels: [this.vwapscale(vwap-200), this.vwapscale(vwap-100), this.vwapscale(vwap+100), this.vwapscale(vwap+200)],  // Print labels at these values
                  title : [vwap-200, vwap-100, vwap+100, vwap+200],
                  color: "#FFFFFF",  // Optional: Label text color
                  fractionDigits: 0  // Optional: Numerical precision. 0=round off.
              },
            };
            var target = document.getElementById('vwapgauge'); // your canvas element
            var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
            gauge.maxValue = this.vwap_scale_max; // set max gauge value
            gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
            gauge.animationSpeed = 1; // set animation speed (32 is default value)
            gauge.set(this.vwapscale(market_price)); // set actual value
        }
}