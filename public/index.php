<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OmniTrader WebApp</title>
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css" >
    <link rel="stylesheet" href="./assets/css/fontawesome.css">
	<link rel="stylesheet" href="./assets/css/element-ui.css">
    <link rel="stylesheet" href="./assets/css/tailwind.css">
	<link rel="stylesheet" href="./assets/css/custom.css">	  
</head>
<body>
<?php 
	$g_userid = "";
	$host = "";
	$ip = "";
	$port = "";
	if (isset($_GET) && sizeof($_GET) > 0){
		$g_userid = $_GET["userid"];
		$host = $_GET["host"];
		$ip = gethostbyname($host);
		$port = $_GET["port"];
	}
?>
	<div id="app">
		<home></home>
	</div>
	<script src="./assets/js/vue.min.js"></script>
    <script src="./assets/js/httpVueLoader.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" ></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js"></script>
	<script src="https://www.altodock.com/majestic/omni/webapp3/alertify/lib/alertify.min.js"></script>	
	<script src="./assets/js/common.js?v=1.191"></script>
	<script src="./assets/js/gauge.min.js"></script>		
	<script src="./assets/js/element-ui.js"></script>
	<script>
		const g_userid = '<?php echo $g_userid;?>';
		const host = '<?php echo $host;?>';
		const ip = '<?php echo $ip;?>';
		const port = '<?php echo $port;?>';
	</script>
	<script type="module" src="main.js"></script>
</body>
</html>
