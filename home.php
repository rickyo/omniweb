<?php
session_start();
ini_set("allow_url_fopen", 1);

include_once("config.php");
include_once("connection.php");


if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniTrader WebApp</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link rel="stylesheet" href="./css/style.css?v=<?php echo date("YmdH"); ?>10">
	<link rel="stylesheet" href="alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />

	 
</head>
<style>
.charthref{padding-right:20px;}
#vwapgauge{position:fixed; top:0; left: 550px;}
</style>
<body>

<?php

		$param = "userid=".$g_userid."&host=".$host."&port=".$port; 
?>
<header>
	<div class="logo"><img src="images/logo.jpg" height="50"></div>

		<?php include("marketHeader.php"); ?>
		
		<div class="connection">
		 <form method="GET" action="" >
			User <input type="text" name="userid" class="usertext" value="<?php echo $g_userid; ?>"></input>
			Host <input type="text" name="host" class="host" value="<?php echo $host; ?>"></input>
			Port <input type="text" name="port" class="port" value="<?php echo $port; ?>"></input>
			<input type="submit" value="Submit" />
		 </form>	
		</div>

</header>
<div class="container">
	

	<div class="container-left">
		
		<iframe id="omni_account" src="accountlist.php?<?php echo $param; ?>" frameborder="0"  width="100%" height="100%" ></iframe>
	</div>	
	<?php 
	if (!(isset($_GET["nopanel"]) && $_GET["nopanel"] == 1)){ ?>
		
	<div class="container-right">
	
	 <div class="upper">
		<div class="graph">
		<?php $chart = "https://tvc4.forexpros.com/init.php?family_prefix=tvc4&time=1479947567&domain_ID=55&lang_ID=55&timezone_ID=28&pair_ID=8984&interval=60&refresh=4&session=24x7&client=1&user=200995618&width=830&height=800&init_page=live-charts&m_pids=";
		
			$msachart = "https://chart.altodock.com";
			

		
		?>
			<a href="<?php echo $chart; ?>" class="charthref" target="chart_iframe"> Full Chart</a>
			<a href="<?php echo $msachart; ?>" class="charthref" target="chart_iframe"> Full MSA Chart</a>
			<a href="tradingPlan.php?userid=<?php echo $g_userid; ?>&host=<?php echo $host; ?>&port=<?php echo $port; ?>" class="charthref"rel="noopener noreferrer" target="_blank"> Trading Plan</a>
			<iframe src="<?php echo $chart; ?>" frameborder="0" name="chart_iframe"  width="100%" height="100%" ></iframe>
			<div class="threshold_list"></div>
		</div>
		<?php /*
		<div class="notifications">
			<iframe src="notificationlist.php" frameborder="0"  width="100%" height="100%" ></iframe>
		</div> */ ?>
	</div>
	<div class="lower">
		<div class="recordlist">
			<iframe id="recordlist"  src="recordlist.php?<?php echo $param; ?>" class="omni_record" frameborder="0"  width="100%" height="100%" ></iframe>
		</div>	
	<?php/*	<div class="controlpanel">
			<?php /* <iframe src="controlpanel.php" frameborder="0"  width="100%" height="100%" ></iframe> 
			 include "ordersetting.php"; 
		</div> */ ?>
	</div>
		
	</div>

	<?php } ?>
	</div>
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
	<script src="alertify/lib/alertify.min.js"></script>	
	<script src="js/jquery.session.js"></script>
	<script src="js/common.js?v=1.191"></script>
	<script src="js/gauge.min.js"></script>
    <script>
		var refresh_rate = <?php echo refresh_rate(2);?>; 
		
		// config
		var elapsed_time_max = -1;
		
		function refreshOrderStat(){
			var accframe = $('#omni_account').contents();
			var userid = accframe.find("select[id=acc_list]").val().trim();
			var this_server = accframe.find("input[name=server]").val();		
			var this_port = accframe.find("input[name=port]").val();
			var rsrc = "recordlist.php?userid="+userid+"&host="+this_server+"&port="+this_port;
			 $('#recordlist').attr("src", rsrc);												
			
		}
		
		$('document').ready(function () {

		$("#demo").val(<?php echo isset($_GET["demo"]) ? $_GET["demo"] : 0; ?>);


		refreshMarketInfo();
		//setInterval(function () {refreshOrderStat()}, refresh_rate);//request every x seconds
		
		
		
		function refreshMarketInfo()
		{
			var accframe = $('#omni_account').contents();
			var recframe = $('.omni_record').contents();
			
			var server = {
				host : "<?php echo $host; ?>",
				ip : "<?php echo $ip;?>",
				port : "<?php echo $port;?>",
				userid : "<?php echo $g_userid;?>"
			};
			
				getMarketData("home.php", server, false, accframe, recframe,refreshMarketInfo );
			
			
				$(accframe).find('#acc_list').change(function(){
				
					var userid = $(this).val();								
					$(accframe).find('.account_list .row').each(function(){
						
						
						$(this).removeClass("parent");
						
						if ($(this).find("input[name=userid]").val().trim() == userid){
												
							var this_server = $(this).find("input[name=server]").val();		
							var this_port = $(this).find("input[name=port]").val();

							$(recframe).find("input[name=userid]").val(userid);
							$(recframe).find("input[name=server]").val(this_server);
							$(recframe).find("input[name=port]").val(this_port);
							
							var rsrc = "recordlist.php?userid="+userid+"&host="+this_server+"&port="+this_port;
							 $('#recordlist').attr("src", rsrc);				
							console.log(rsrc);
							
							$(this).addClass("parent");
						}
					});
				 
				});
				
			}

			
		});
		
		 
		 
		 
	</script> 

</body>
</html>