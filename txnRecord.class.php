<?php

class TxnRecord
{

	
	public function __construct($registry) {
		$this->db = $registry->get('db');
		
	}
	
	public function getRecord($criteria=array())
	{
	
			$sql = "SELECT * FROM omni_txn_log ";
		
			$data_arr = array();
			if (isset($criteria["date_fr"])){
				array_push($data_arr, "`date` >= '".$this->db->real_escape_string(trim($criteria["date_fr"]))." 00:00:00'");
			}
			
			if (isset($criteria["date_to"])){
				array_push($data_arr, "`date` <= '".$this->db->real_escape_string(trim($criteria["date_to"]))." 23:59:59'");
			}
			
			if (sizeof($data_arr) > 0){
				$sql .= " WHERE ".implode(" AND ", $data_arr);
			}
		
			$sql .= " ORDER BY `date` DESC ";
			
					
			//echo $sql."<br/>";
			$result = getQuery($sql);
			
			return $result;
	}

	public function create($data = array())
	{


	 $txn_data = array();
		
	  $txn_data["log_id"] = "NULL";
	  $txn_data["userid"] = isset($data["userid"])? "'".$this->db->real_escape_string(trim($data["userid"]))."'" : "''";
	  $txn_data["code"] = isset($data["code"])? "'".$this->db->real_escape_string(trim($data["code"]))."'" : "''";
	  $txn_data["position"] = isset($data["position"])? "'".$this->db->real_escape_string(trim($data["position"]))."'" : "''";
	   $txn_data["price"] = isset($data["price"])? "'".$this->db->real_escape_string(trim($data["price"]))."'" : "''";

	  $txn_data["cut_gain"] =  isset($data["cut_gain"])? intval($data["cut_gain"]) : 0;
	  $txn_data["cut_loss"] =  isset($data["cut_loss"])? intval($data["cut_loss"]) : 0;
	  $txn_data["threshold"] =  isset($data["threshold"])? intval($data["threshold"]) : 0;
	  $txn_data["date"] = isset($data["date"])? (strtolower($data["date"]) == "now()" ? "NOW()" : "'".$this->db->real_escape_string(trim($data["date"]))."'" ) : "'0000-00-00 00:00:00'";
	 $txn_data["settle_point"] =  isset($data["settle_point"])? intval($data["settle_point"]) : 0;
	 $txn_data["settle_balance"] =  isset($data["settle_balance"])? floatval($data["settle_balance"]) : 0;

		$sql = "INSERT INTO "."omni_txn_log"." (";
		$sql .= implode(",", array_keys($txn_data));
		$sql .= ") VALUES (";
		$sql .= implode("," ,$txn_data);
		$sql .= ")";
	   
	   
		$result = getQuery($sql);
		$txn_id = getInsertID();
		
		return $txn_id;
		
	}
	
	public function update($criteria = array())
	{
		$fields = array();
		
	/*	if (isset($criteria["campaign_name"])  && trim($criteria["campaign_name"]) != "''"){
			$fields["campaign_name"] = ( trim($criteria["campaign_name"]) == "''" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["campaign_name"]))."'");
		}
		
		if (isset($criteria["source"])  && trim($criteria["source"]) != "''"){
			$fields["source"] = ( trim($criteria["source"]) == "''" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["source"]))."'");
		}
		
		if (isset($criteria["url"])  && trim($criteria["url"]) != "''"){
			$fields["url"] = ( trim($criteria["url"]) == "''" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["url"]))."'");
		}
		
		if (isset($criteria["tracking_url"])  && trim($criteria["tracking_url"]) != "''"){
			$fields["tracking_url"] = ( trim($criteria["tracking_url"]) == "''" ? "NULL" : "'".$this->db->real_escape_string(trim($criteria["tracking_url"]))."'");
		}
		
		if (isset($criteria["count"]) && intval($criteria["count"]) > 0){
			$fields["count"] =  "count + 1";
		}
		
		if (isset($criteria["status"])  && trim($criteria["status"]) != "''"){
			$fields["status"] = intval($criteria["status"]);
		}
		*/
		
		if (sizeof($fields) > 0)
        {		
				
            $sql = "UPDATE omni_txn_log SET ";
            $tmp = array();
            foreach ($fields as $title => $val){
				$field_data_arr[$title] = $val;
                array_push($tmp , $title." = ".$val);
            }
            $sql .= implode(",", $tmp);
           
            $condition = array();
                               
            if (isset($criteria["log_id"]))
                array_push($condition,  "log_id = ".$criteria["log_id"]);
	              
			if (sizeof($condition) > 0)
				$sql .= " WHERE ".implode(" AND ", $condition);

            $result = getQuery($sql);
			

        //    echo $sql;
           
            //$result = $this->getConnection()->query($sql);
        }
	}

	
	
}

?>

