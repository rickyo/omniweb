<?php

ini_set("allow_url_fopen", 1);

include_once("config.php");
include_once("connection.php");


if (isset($_GET) && sizeof($_GET) > 0){
	$g_userid = $_GET["userid"];

	$host = $_GET["host"];
	$ip = gethostbyname($host);
	$port = $_GET["port"];
}

$refresh_rate = 2;
$refresh_rate_vwap = 10;
$refresh_rate_tradenorm = 11;
if (isset($_GET["refresh_rate"]) && $_GET["refresh_rate"] != ""){
	list($refresh_rate, $refresh_rate_vwap, $refresh_rate_tradenorm) =  explode(",", $_GET["refresh_rate"]);
}

?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo csstransforms csstransforms3d csstransitions" slick-uniqueid="3">
<head>
    <!-- Site Title-->
    <title>OmniTrader WebApp Admin</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	 <link rel="stylesheet" href="./css/admin.css?v=<?php echo date("YmdHis"); ?>">
	<link rel="stylesheet" href="alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="alertify/themes/alertify.default.css" id="toggleCSS" />

	 
</head>

<body>

<?php

	$conn_r = array("host" => $host, "ip" => $ip, "port" => $port);	
	$json = array();
	$json["account"] = $g_userid;
	$json["action"] = "MARKET_DATA";
	$json["value"] = "TRUE";


	try {
	   
		$connection = new Connection();
		$connection->setconnect($conn_r);
		$result =  $connection->sendcommand($json);	
		
		$close_net = "-";
		
		if ($result != ""){
			$market_data = json_decode($result);
			$close_net = $market_data ? $market_data->close - $market_data->prevClose : "-";
		}
		
		
		$param = "userid=".$g_userid."&host=".$host."&port=".$port; 
		
	} catch (Exception $e) {
		echo "<p class=\"error\">ERROR - Connection Failure </p>";
	}
	
	
	
?>

<style>
body {width:480px;} 
.title {color:#000000; text-transform: uppercase; background:#b8671d;  padding:5px;}
.box {display:block; width:80%; margin : 0 15px;padding: 10px 0; }

.align-left {    vertical-align: top;}
.col-50{ height:50px;}
.contentbox{background:#1b1405; border:1px solid #b8671d; }
.container{padding:10px;}
#connprofile span{color:#FFFFFF; display:inline-block;}
#connprofile input{color:#FFFFFF; background:#1b1405; border: 1px solid #CCCCCC;}
button {   -webkit-appearance: button; overflow: visible;  text-transform: none; font: inherit;} 
#autoreload_time{color:#FFFFFF;}

</style>
<?php
	if (!(isset($_GET["noheader"]) && $_GET["noheader"] == 1)){
 ?>
	<header>
	<div class="box col-50">
		<div class="logo"><img src="images/logo.jpg" height="50"> </div>
		<h1>Omni Admin Panel</h1>
	</div>
	</header>
	<div class="clearfix"></div>
<?php	} ?>


	<div id="marketinfo" class="box ">
			<div class="contentbox">
			
			<div class="title"> Admin Panel </div>
			<div class="container">
			
			
			<?php include("marketHeader.php"); ?>
			
			
			</div>
			
			</div>
	</div>		
		<div class="box col-50">
		<div class="align-left" >
		 <form method="GET" action=""  id="connprofile">
			<span>User</span> <input type="text" name="userid" class="usertext" value="<?php echo $g_userid; ?>"></input>
			<span>Host</span> <input type="text" name="host" class="host" value="<?php echo $host; ?>"></input>
			<span>Port</span> <input type="text" name="port" class="port" value="<?php echo $port; ?>"></input>
			<input type="submit" value="Submit" />
		 </form>
		 </div>
		<div class="align-right" >
		 	<div class="adminpanel">
				
				<div id="autoreload_time" ></div>
			
			
				<button type="submit" id="cp_refresh_mkt_data" value="refresh_mkt_data"> Refresh Market Data </button>
				<br/>
				<input type="checkbox" name="autoExecVwap" value="1"  /><span style="color:#FFF;"> Auto Refresh </span>
			</div>
		 </div>
		</div>


	<div class="clearfix"></div>



	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>	
	<script src="js/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
	<script src="alertify/lib/alertify.min.js"></script>
	<script src="js/jquery.session.js"></script>
	<script src="js/common.js?v1.3"></script>
	<script src="js/gauge.min.js"></script>
    <script>
		
	
	
	
		var refresh_rate = <?php echo refresh_rate($refresh_rate);?>; 
		var refresh_rate_vwap = <?php echo refresh_rate($refresh_rate_vwap);?>; 
		var refresh_rate_tradenorm = <?php echo refresh_rate($refresh_rate_tradenorm);?>; 
		
		var vwapcheck = 0;
		var vwapelapsed = 0;
		var vwaptime_threshold = 30; // check vwap no respond after 30 sec
		
		var tradenormcheck = 0;
		var tradenormelapsed = 0;
		var tradenormtime_threshold = 20;
		
		// config
		var elapsed_time_max = 120; // over 120 min - stop check 
		
	$('document').ready(function () {
		

		var accframe = $('#omni_account').contents();		
		var recframe = $('.omni_record').contents();
		
		var url = "OmniController.php"; 		
		
		var server = {
			host : "<?php echo $host; ?>",
			ip : "<?php echo $ip;?>",
			port : "<?php echo $port;?>",
			userid : "<?php echo $g_userid;?>"
		};
			
		getMarketData("admin.php", server, true, accframe, recframe);	
		
		showVwapGauge();
			
		setInterval(function () {getMarketData("admin.php", server, true, accframe, recframe)}, refresh_rate);//request every x seconds
		setInterval(function () {checkVwapHeartBeat()}, refresh_rate_vwap);//request every x seconds
		setInterval(function () {checkNominal()}, refresh_rate_tradenorm);//request every x seconds
		
		
		function checkNominal(){
			var tradenormvalue = parseFloat($("#tradenorm").text());
			console.log("Tracking Trade Nominal ... "+ tradenormvalue + " Elapsed time: " + tradenormelapsed);
			
			if (tradenormcheck == tradenormvalue){
				tradenormelapsed += refresh_rate_tradenorm/1000;
				if (tradenormelapsed >= tradenormtime_threshold){
					
					var objDate = new Date();
					var hours = objDate.getHours();
					var weekday = objDate.getDay();
					//if ((hours >= 9 && hours <= 12) || (hours >= 13 && hours <= 17)){
					if ((hours >= 9 && hours < 12) || (hours >= 13 && hours <= 23)){	//testing tobe remove
						
						if (weekday >= 1 && weekday <= 5){
					
							alertify.error("["+objDate.getHours() + ":"+ objDate.getMinutes ()+ ":"+ objDate.getSeconds()+ "] Warning : Trade Nominal Stopped <br>("+tradenormelapsed +" sec)");
						
							refreshMarketData(server, 1);
						
						}
					}else{
						console.log("Warning : tradenorm Stopped ("+tradenormelapsed +" sec)");
					}
					
					
				}
			}else{
				tradenormelapsed = 0;
				tradenormcheck = tradenormvalue;
			}
		}
		
		
		function checkVwapHeartBeat(){
			

			var vwapvalue = parseFloat($("#marketheader .vwap span").text())
			console.log("Tracking vwap ... " + vwapvalue + " Elapsed time: "+ vwapelapsed);
			
			if (vwapcheck == vwapvalue){
				vwapelapsed += refresh_rate_vwap/1000;
				if (vwapelapsed >= vwaptime_threshold){
					
					var objDate = new Date();
					var hours = objDate.getHours();
					var weekday = objDate.getDay();
					if ((hours >= 9 && hours < 12) || (hours >= 13 && hours <= 17)){
						
						if (weekday >= 1 && weekday <= 5){
					
							alertify.error("["+objDate.getHours() + ":"+ objDate.getMinutes ()+ ":"+ objDate.getSeconds()+ "] Warning : VWAP Stopped <br>("+vwapelapsed +" sec)");
						
							refreshMarketData(server, 1);
						
						}
					}else{
						console.log("Warning : VWAP Stopped <br>("+vwapelapsed +" sec)");
					}
					
					
				}
			}else{
				vwapelapsed = 0;
				vwapcheck = vwapvalue;
			}
				
		}
		
	
	
		
		$("#cp_refresh_mkt_data").click(function(){
				
			console.log(" Action : Refresh Market Data Clicked ");	
			refreshMarketData(server, 0);
		
		});
		
		 
	});		 
		 
	</script> 

</body>
</html>